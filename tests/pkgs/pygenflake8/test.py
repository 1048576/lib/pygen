from __future__ import annotations

import unittest

from typing import Final

import pygenflake8

from pygenast.utils import ASTUtils
from pygencollection.impl import ArrayImpl
from pygencollection.impl import CollectionEncoderImpl
from pygendataclass.impl import DataclassEncoderImpl
from pygenflake8.impl import Flake8CheckerImpl
from pygenjson.abc import JsonMapper
from pygenjson.impl import PrettyJsonMapperImpl
from pygenos.abc import OSDir
from pygenpath.utils import PathUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestsample.utils import SampleTestUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class Flake8CheckerTestCaseImpl(TestCase):
    _json_mapper: Final[JsonMapper]
    _expected_code: Final[str]
    _dir: Final[OSDir]

    def __init__(
        self,
        dirname: str,
        dir: OSDir
    ) -> None:
        self._json_mapper = PrettyJsonMapperImpl(
            encoders=ArrayImpl.of(
                CollectionEncoderImpl(),
                DataclassEncoderImpl()
            )
        )
        self._expected_code = dirname
        self._dir = dir

    def run(self, test: Test) -> None:
        source_filepath = self._dir.dirpath.resolve_filepath(
            path="./source.py"
        )
        report_filepath = self._dir.dirpath.resolve_filepath(
            path="./report.json"
        )

        with (open(source_filepath.path()) as f):
            lines = f.readlines()
            source = TextUtils.join("", lines)

        tree = ASTUtils.parse(source, source_filepath.path())

        checker = Flake8CheckerImpl(
            tree=tree,
            filename=source_filepath.path(),
            lines=lines
        )

        report = ArrayImpl.copy(
            instance=checker.run()
        )

        for entry in report:
            test.assert_text_equal(
                expected=self._expected_code,
                actual=entry.text[:entry.text.find(" ")],
                msg=lambda args: TextUtils.format_args(
                    tpl="Invalid code [{}:{}:{}]\n{}",
                    args=[
                        source_filepath,
                        entry.line,
                        entry.column,
                        args.diff
                    ]
                )
            )

        with (open(report_filepath.path()) as f):
            expected = f.read()

            test.assert_text_equal(
                expected=expected,
                actual=self._json_mapper.to_text(report),
                msg=lambda args: TextUtils.format_args(
                    tpl="expected != actual [{}]\n{}",
                    args=[
                        self._dir.dirpath,
                        args.diff
                    ]
                )
            )

        test.assert_text_equal(
            expected=self._json_mapper.to_text(sorted(self._dir.filenames)),
            actual=self._json_mapper.to_text(
                instance=[
                    "report.json",
                    "source.py"
                ]
            ),
            msg=lambda args: TextUtils.format_args(
                tpl="expected != actual [{}]:\n{}",
                args=[
                    self._dir.dirpath,
                    args.diff
                ]
            )
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                __file__
            ]
        )
    )
    workspace_dirpath = dirpath.resolve_dirpath("../../../../")

    test_suite = UnitTestSuiteImpl()

    SampleTestUtils.load(
        workspace_dirpath=workspace_dirpath,
        module=pygenflake8,
        create_test_case_fn=Flake8CheckerTestCaseImpl,
        test_suite=test_suite
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
