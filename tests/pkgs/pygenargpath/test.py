from __future__ import annotations

import unittest

from typing import Final
from typing import Generic

from pygenargpath.abc import PathArg
from pygenargpath.impl import PathArgImpl
from pygenpath.abc import PathT
from pygenpath.utils import PathUtils
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class PathArgTestCaseImpl(Generic[PathT], TestCase):
    @classmethod
    def create_dirpath_test_case(
        cls,
        description: str,
        salt: str
    ) -> TestCase:
        return PathArgTestCaseImpl(
            description=description,
            value=PathUtils.dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        salt
                    ]
                )
            )
        )

    @classmethod
    def create_filepath_test_case(
        cls,
        description: str,
        salt: str
    ) -> TestCase:
        return PathArgTestCaseImpl(
            description=description,
            value=PathUtils.filepath(
                path=TextUtils.format_args(
                    tpl="./{}",
                    args=[
                        salt
                    ]
                )
            )
        )

    _description: Final[str]
    _value: Final[PathT]
    _arg: Final[PathArg[PathT]]

    def __init__(self, description: str, value: PathT) -> None:
        self._description = description
        self._value = value
        self._arg = PathArgImpl(
            description=self._description,
            value=self._value
        )

    def run(self, test: Test) -> None:
        self._test_value(test)
        self._test_describe(test)

    def _test_describe(self, test: Test) -> None:
        with (test.sub_test(PathArg.describe.__name__)):
            test.assert_text_equal(
                expected=TextUtils.format_args(
                    tpl="{}: [{}]",
                    args=[
                        self._description,
                        self._value.path()
                    ]
                ),
                actual=self._arg.describe()
            )

    def _test_value(self, test: Test) -> None:
        with (test.sub_test(PathArg.value.__name__)):
            arg_ctx = self._arg.__enter__()

            test.assert_object_equal(
                expected=self._value,
                actual=arg_ctx.value()
            )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    text_generator = UniqueRandomTextGeneratorImpl()

    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=PathArgTestCaseImpl.create_dirpath_test_case(
            description=text_generator.randtext(),
            salt=text_generator.randtext()
        )
    )
    test_suite.add_test_case(
        test_case=PathArgTestCaseImpl.create_filepath_test_case(
            description=text_generator.randtext(),
            salt=text_generator.randtext()
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
