from __future__ import annotations

import unittest

from collections.abc import Callable
from collections.abc import Iterator
from typing import Final

import pygenyamldoclexer
import pygenyamldoclexer.processor
import pygenyamldoclexer.processor.dictkey

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenos.abc import OSDir
from pygenos.utils import OSUtils
from pygenpath.abc import Dirpath
from pygenpath.utils import PathUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentestunknowndir.impl import UnknownDirTestCaseImpl
from pygentext.utils import TextUtils


class YamlDocLexerSamplesDirTestCaseImpl(TestCase):
    _tests_dir_dirpath: Final[Dirpath]
    _samples_dir_dirpath: Final[Dirpath]

    def __init__(self, workspace_dirpath: Dirpath) -> None:
        self._tests_dir_dirpath = workspace_dirpath.resolve_dirpath(
            path=TextUtils.format_args(
                tpl="./tests/pkgs/{}/",
                args=[
                    TextUtils.replace(
                        text=pygenyamldoclexer.processor.__name__,
                        old=".",
                        new="/"
                    )
                ]
            )
        )
        self._samples_dir_dirpath = workspace_dirpath.resolve_dirpath(
            path=TextUtils.format_args(
                tpl="./samples/pkgs/{}/",
                args=[
                    TextUtils.replace(
                        text=pygenyamldoclexer.processor.__name__,
                        old=".",
                        new="/"
                    )
                ]
            )
        )

    def run(self, test: Test) -> None:
        expected_dirpaths = ArrayImpl.collect(
            iterator=self._dirpaths(
                dirs=OSUtils.walk(self._tests_dir_dirpath),
                filter_fn=lambda dir: ("test.py" in dir.filenames)
            )
        )
        actual_dirpaths = CollectionUtils.to_list(
            items=self._dirpaths(
                dirs=OSUtils.walk(self._samples_dir_dirpath),
                filter_fn=lambda dir: any(dir.filenames)
            )
        )
        unknown_dirpaths = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=actual_dirpaths,
                fn=lambda dirpath: (dirpath not in expected_dirpaths)
            )
        )

        test_case = UnknownDirTestCaseImpl(unknown_dirpaths)

        test_case.run(test)

    def _dirpaths(
        self,
        dirs: Iterator[OSDir],
        filter_fn: Callable[[OSDir], bool]
    ) -> Iterator[Dirpath]:
        root_dir = next(dirs)
        other_dirs = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=dirs,
                fn=filter_fn
            )
        )

        for dirname in root_dir.dirnames:
            dirpath = root_dir.dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        dirname
                    ]
                )
            )

            if (dirpath.resolve_dirpath("../") != root_dir.dirpath):
                continue

            for other_dir in other_dirs:
                if (dirpath.path() in other_dir.dirpath.path()):
                    yield self._samples_dir_dirpath.resolve_dirpath(
                        path=TextUtils.format_args(
                            tpl="./{}/",
                            args=[
                                dirname
                            ]
                        )
                    )

                    break


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                __file__
            ]
        )
    )
    workspace_dirpath = dirpath.resolve_dirpath("../../../../../")

    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=YamlDocLexerSamplesDirTestCaseImpl(
            workspace_dirpath=workspace_dirpath
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
