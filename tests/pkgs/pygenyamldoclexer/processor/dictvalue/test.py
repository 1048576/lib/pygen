from __future__ import annotations

import unittest

from collections.abc import Mapping
from dataclasses import dataclass
from typing import Any
from typing import Final

import pygenyamldoclexer
import pygenyamldoclexer.processor
import pygenyamldoclexer.processor.dictvalue

from pygenos.abc import OSDir
from pygenpath.utils import PathUtils
from pygentestsample.utils import SampleTestUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentextreader.abc import TextReader
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.fn.impl import YamlDocLexerFnMapperImpl
from pygenyamldoclexer.processor.dictvalue.impl import YamlDocLexerDictValueProcessorImpl
from pygenyamldoclexer.processor.impl import YamlDocLexerProcessorMapperImpl
from pygenyamldoclexer.result.impl import YamlDocLexerResultMapperImpl
from pygenyamldoclexer.value.impl import YamlDocLexerValueMapperImpl
from pygenyamldoclexerdev.abc import YamlDocLexerValueCollection
from pygenyamldoclexerdev.impl import YamlDocLexerValueListImpl
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseArgsFn
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseEntrypointFn
from pygenyamldoclexerdev.testcase.impl import YamlDocLexerTestCaseImpl
from pygenyamldoclexerdev.testcase.impl import YamlDocLexerTestCaseResultMapperImpl


@dataclass
class YamlDocLexerDictValueProcessorTestCaseArgs(object):
    indent: Final[int]


class YamlDocLexerDictValueProcessorTestCaseImpl(
    YamlDocLexerTestCaseImpl[YamlDocLexerDictValueProcessorTestCaseArgs]
):
    class ArgsFnImpl(
        YamlDocLexerTestCaseArgsFn[
            YamlDocLexerDictValueProcessorTestCaseArgs
        ]
    ):
        def __call__(
            self,
            args: Mapping[str, Any]
        ) -> YamlDocLexerDictValueProcessorTestCaseArgs:
            return YamlDocLexerDictValueProcessorTestCaseArgs(**args)

    class EntrypointFnImpl(
        YamlDocLexerTestCaseEntrypointFn[
            YamlDocLexerDictValueProcessorTestCaseArgs
        ]
    ):
        def __call__(
            self,
            args: YamlDocLexerDictValueProcessorTestCaseArgs,
            reader: TextReader
        ) -> YamlDocLexerValueCollection:
            values = YamlDocLexerValueListImpl()
            value_mapper = YamlDocLexerValueMapperImpl()
            result_mapper = YamlDocLexerTestCaseResultMapperImpl(
                values=values,
                origin=YamlDocLexerResultMapperImpl(value_mapper)
            )

            proccessor = YamlDocLexerDictValueProcessorImpl(
                result_mapper=result_mapper,
                value_mapper=value_mapper,
                processor_mapper=YamlDocLexerProcessorMapperImpl(
                    result_mapper=result_mapper,
                    fn_mapper=YamlDocLexerFnMapperImpl(),
                    value_mapper=value_mapper
                ),
                end_dict_processor=YamlDocLexerProcessor(),
                indent=args.indent
            )

            return YamlDocLexerValueListImpl.of(
                proccessor.process(reader).value()
            )

    def __init__(self, dirname: str, dir: OSDir) -> None:
        super().__init__(
            args_fn=self.ArgsFnImpl(),
            entrypoint_fn=self.EntrypointFnImpl(),
            dirname=dirname,
            dir=dir
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                __file__
            ]
        )
    )
    workspace_dirpath = dirpath.resolve_dirpath("../../../../../../")

    test_suite = UnitTestSuiteImpl()

    SampleTestUtils.load(
        workspace_dirpath=workspace_dirpath,
        module=pygenyamldoclexer.processor.dictvalue,
        create_test_case_fn=YamlDocLexerDictValueProcessorTestCaseImpl,
        test_suite=test_suite
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
