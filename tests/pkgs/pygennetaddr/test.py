from __future__ import annotations

import unittest

from pygencollection.impl import ArrayImpl
from pygennetaddr.impl import IPSetImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class IpSetOperatorTestCaseImpl(TestCase):
    def run(self, test: Test) -> None:
        self._test_union(test)
        self._test_intersection(test)

    def _test_intersection(self, test: Test) -> None:
        samples = ArrayImpl.of(
            ("192.168.0.0/24", "192.168.0.0/25", "192.168.0.0/25"),
            ("127.0.128.0/17", "127.0.128.0/18", "127.0.128.0/18"),
            ("10.64.0.0/10", "10.64.0.0/11", "10.64.0.0/11"),
            ("252.0.0.0/6", "254.0.0.0/7", "254.0.0.0/7")
        )

        for a, b, c in samples:
            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="{} intersection {} = {}",
                    args=[a, b, c]
                )
            )

            with (sub_test):
                ip_set_a = IPSetImpl.create([a])
                ip_set_b = IPSetImpl.create([b])
                ip_set_c = ip_set_a.intersection(ip_set_b)

                test.assert_text_equal(
                    expected=TextUtils.format_args(
                        tpl="IPSet(['{}'])",
                        args=[
                            c
                        ]
                    ),
                    actual=str(ip_set_c)
                )

    def _test_union(self, test: Test) -> None:
        samples = ArrayImpl.of(
            ("192.168.0.0/25", "192.168.0.128/25", "192.168.0.0/24"),
            ("127.0.128.0/18", "127.0.192.0/18", "127.0.128.0/17"),
            ("10.64.0.0/11", "10.96.0.0/11", "10.64.0.0/10"),
            ("252.0.0.0/7", "254.0.0.0/7", "252.0.0.0/6")
        )

        for a, b, c in samples:
            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="{} union {} = {}",
                    args=[a, b, c]
                )
            )

            with (sub_test):
                ip_set_a = IPSetImpl.create([a])
                ip_set_b = IPSetImpl.create([b])
                ip_set_c = ip_set_a.union(ip_set_b)

                test.assert_text_equal(
                    expected=TextUtils.format_args(
                        tpl="IPSet(['{}'])",
                        args=[
                            c
                        ]
                    ),
                    actual=str(ip_set_c)
                )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=IpSetOperatorTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
