from __future__ import annotations

import unittest

from typing import Final
from typing import Generic

from pygenarg.abc import Arg
from pygenarg.abc import FinalArg
from pygenargnet.impl import NetworkAddressArgImpl
from pygenargpath.impl import PathArgImpl
from pygenargtext.utils import TextArgUtils
from pygenerr.err import UserException
from pygennet.impl import NetworkAddressImpl
from pygenpath.utils import PathUtils
from pygenrandom.randtext.abc import UniqueRandomTextGenerator
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygenrandom.utils import RandomUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentest.utils import TestUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentype.abc import T


class ArgWithTestCaseImpl(TestCase, Generic[T]):
    class SomeUserException(UserException):
        ...

    _arg: FinalArg[T]
    _user_exception: Final[UserException]
    _system_exception: Final[Exception]

    def __init__(
        self,
        text_generator: UniqueRandomTextGenerator,
        arg: Arg[T]
    ) -> None:
        self._arg = arg
        self._user_exception = self.SomeUserException(
            msg=text_generator.randtext()
        )
        self._system_exception = Exception(text_generator.randtext())

    def run(self, test: Test) -> None:
        self._test_user_exception(test)
        self._test_system_exception(test)

    def _test_system_exception(self, test: Test) -> None:
        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="{}: system exception",
                args=[
                    self._arg.__class__.__name__
                ]
            )
        )

        with (sub_test):
            exception_catcher = test.exception_catcher()

            with (exception_catcher):
                with (self._arg):
                    TestUtils.raise_exception(self._system_exception)

            test.assert_exception_equal(
                expected=self._system_exception,
                exception_catcher=exception_catcher
            )

    def _test_user_exception(self, test: Test) -> None:
        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="{}: user exception",
                args=[
                    self._arg.__class__.__name__
                ]
            )
        )

        with (sub_test):
            exception_catcher = test.exception_catcher()

            with (exception_catcher):
                with (self._arg):
                    TestUtils.raise_exception(self._user_exception)

            test.assert_exception_equal(
                expected=self._user_exception.wrap(
                    title=self._arg.describe()
                ),
                exception_catcher=exception_catcher
            )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    text_generator = UniqueRandomTextGeneratorImpl()

    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=ArgWithTestCaseImpl(
            text_generator=text_generator,
            arg=NetworkAddressArgImpl(
                description=text_generator.randtext(),
                value=NetworkAddressImpl(
                    host=text_generator.randtext(),
                    port=RandomUtils.randint(0, 65536)
                )
            )
        )
    )
    test_suite.add_test_case(
        test_case=ArgWithTestCaseImpl(
            text_generator=text_generator,
            arg=PathArgImpl(
                description=text_generator.randtext(),
                value=PathUtils.dirpath(
                    path=TextUtils.format_args(
                        tpl="./{}/",
                        args=[
                            text_generator.randtext()
                        ]
                    )
                )
            )
        )
    )
    test_suite.add_test_case(
        test_case=ArgWithTestCaseImpl(
            text_generator=text_generator,
            arg=PathArgImpl(
                description=text_generator.randtext(),
                value=PathUtils.filepath(
                    path=TextUtils.format_args(
                        tpl="./{}",
                        args=[
                            text_generator.randtext()
                        ]
                    )
                )
            )
        )
    )
    test_suite.add_test_case(
        test_case=ArgWithTestCaseImpl(
            text_generator=text_generator,
            arg=TextArgUtils.optional(
                description=text_generator.randtext(),
                value=text_generator.randtext()
            )
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
