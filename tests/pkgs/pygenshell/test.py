from __future__ import annotations

import unittest

from collections.abc import Iterable
from contextlib import AbstractContextManager
from dataclasses import dataclass
from threading import Thread
from typing import Final

from pygencollection.abc import FinalList
from pygencollection.abc import List
from pygencollection.impl import ArrayImpl
from pygencollection.impl import ListImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import UserException
from pygennet.abc import NetworkAddress
from pygennet.impl import NetworkAddressImpl
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygenrandom.utils import RandomUtils
from pygenshell.abc import ShellOutput
from pygenshell.abc import ShellOutputEntry
from pygenshell.abc import ShellOutputSource
from pygenshellclient.abc import ShellClient
from pygenshellclient.impl import ShellClientImpl
from pygenshellserver.abc import ShellServerProcessor
from pygenshellserver.impl import ShellServerImpl
from pygensocket.abc import ServerSocket
from pygensocket.impl import ServerSocketImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class ShellTestCaseServerImpl(Thread, AbstractContextManager[None]):
    _server_socket: Final[ServerSocket]
    _processor: Final[ShellServerProcessor]

    def __init__(
        self,
        processor: ShellServerProcessor
    ) -> None:
        super().__init__()

        self._server_socket = ServerSocketImpl()
        self._processor = processor

    def __enter__(self) -> None:
        self._server_socket.bind(
            address=NetworkAddressImpl("127.0.0.1", 0)
        )
        self._server_socket.listen(1)
        self.start()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._server_socket.close()

    def address(self) -> NetworkAddress:
        return self._server_socket.address()

    def run(self) -> None:
        socket = self._server_socket.accept()

        server = ShellServerImpl(
            socket=socket,
            processor=self._processor
        )

        with (socket):
            server.serve_forever()


@dataclass
class ShellTestCaseCall(object):
    args: FinalCollection[str]


@dataclass
class ShellTestCaseCallOutput(object):
    entries: FinalCollection[ShellOutputEntry]


class ShellPositiveTestCaseImpl(TestCase):
    class ShellServerProcessorImpl(ShellServerProcessor):
        _call_log: FinalList[ShellTestCaseCall]
        _call_outputs: FinalList[ShellTestCaseCallOutput]

        def __init__(
            self,
            call_log: List[ShellTestCaseCall],
            call_outputs: Iterable[ShellTestCaseCallOutput]
        ) -> None:
            self._call_log = call_log
            self._call_outputs = CollectionUtils.to_list(call_outputs)

        def process(
            self,
            args: Iterable[str],
            output: ShellOutput
        ) -> int:
            self._call_log.add(
                value=ShellTestCaseCall(
                    args=ArrayImpl.copy(args)
                )
            )

            call_output = self._call_outputs.detach(0)

            for entry in call_output.entries:
                output.write(entry)

            return 0

    class ShellOutputImpl(ShellOutput):
        _output_log: FinalList[ShellOutputEntry]

        def __init__(self, output_log: List[ShellOutputEntry]) -> None:
            self._output_log = output_log

        def write(self, entry: ShellOutputEntry) -> None:
            self._output_log.add(
                value=entry
            )

    @classmethod
    def to_text(cls, items: Iterable[object]) -> str:
        return TextUtils.join(
            "\n",
            items=CollectionUtils.map(
                items=items,
                fn=lambda item: repr(item)
            )
        )

    _calls: FinalCollection[ShellTestCaseCall]
    _call_outputs: FinalCollection[ShellTestCaseCallOutput]
    _client: Final[ShellClient]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._calls = ArrayImpl.of(
            ShellTestCaseCall(
                args=ArrayImpl.of(
                    text_generator.randtext()
                )
            ),
            ShellTestCaseCall(
                args=ArrayImpl.of(
                    text_generator.randtext(),
                    text_generator.randtext()
                )
            )
        )
        self._call_outputs = ArrayImpl.of(
            ShellTestCaseCallOutput(
                entries=ArrayImpl.of(
                    ShellOutputEntry(
                        source=RandomUtils.choice(
                            values=ArrayImpl.copy(
                                instance=ShellOutputSource
                            )
                        ),
                        data=text_generator.randtext()
                    )
                )
            ),
            ShellTestCaseCallOutput(
                entries=ArrayImpl.of(
                    ShellOutputEntry(
                        source=ShellOutputSource.STDERR,
                        data=text_generator.randtext()
                    ),
                    ShellOutputEntry(
                        source=ShellOutputSource.STDOUT,
                        data=text_generator.randtext()
                    )
                )
            )
        )
        self._client = ShellClientImpl()

    def run(self, test: Test) -> None:
        test.assert_scalar_equal(
            expected=len(self._calls),
            actual=len(self._call_outputs)
        )

        call_log = ListImpl[ShellTestCaseCall]()

        server = ShellTestCaseServerImpl(
            processor=self.ShellServerProcessorImpl(
                call_log=call_log,
                call_outputs=self._call_outputs
            )
        )

        with (server):
            with (self._client.connect(server.address()) as session):
                pairs = CollectionUtils.join_by_index(
                    first=self._calls,
                    second=self._call_outputs
                )

                for call, call_output in pairs:
                    output_log = ListImpl[ShellOutputEntry]()

                    session.call(
                        args=call.args,
                        output=self.ShellOutputImpl(
                            output_log=output_log
                        )
                    )

                    test.assert_text_equal(
                        expected=self.to_text(call_output.entries),
                        actual=self.to_text(output_log)
                    )

        test.assert_text_equal(
            expected=self.to_text(self._calls),
            actual=self.to_text(call_log)
        )


class ShellNegativeTestCaseImpl(TestCase):
    class ShellServerProcessorImpl(ShellServerProcessor):
        _status: Final[int]

        def __init__(self, status: int) -> None:
            self._status = status

        def process(
            self,
            args: Iterable[str],
            output: ShellOutput
        ) -> int:
            return self._status

    class ShellOutputImpl(ShellOutput):
        def write(self, entry: ShellOutputEntry) -> None:
            ...

    _status: Final[int]
    _client: Final[ShellClient]

    def __init__(self) -> None:
        self._status = RandomUtils.randint(1, 256)
        self._client = ShellClientImpl()

    def run(self, test: Test) -> None:
        server = ShellTestCaseServerImpl(
            processor=self.ShellServerProcessorImpl(
                status=self._status
            )
        )

        with (server):
            exception_catcher = test.exception_catcher(UserException)

            with (exception_catcher):
                with (self._client.connect(server.address()) as session):
                    session.call(
                        args=[],
                        output=self.ShellOutputImpl()
                    )

        test.assert_exception_equal(
            expected=UserException(
                msg=TextUtils.format_args(
                    tpl="Return code [{}]",
                    args=[
                        self._status
                    ]
                )
            ),
            exception_catcher=exception_catcher
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=ShellPositiveTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=ShellNegativeTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
