from __future__ import annotations

import random
import unittest

from typing import Final

from pygenerr.err import UserException
from pygennet.abc import NetworkAddress
from pygennet.impl import NetworkAddressImpl
from pygennet.utils import NetworkUtils
from pygenrandom.utils import RandomUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class NetworkUtilsAddressTestCaseImpl(TestCase):
    _expected: Final[NetworkAddress]

    def __init__(self) -> None:
        self._expected = NetworkAddressImpl(
            host=RandomUtils.randtext(),
            port=random.randint(0, 65536)
        )

    def run(self, test: Test) -> None:
        actual = NetworkUtils.address(
            value=TextUtils.format_args(
                tpl="{}:{}",
                args=self._expected.args()
            )
        )

        test.assert_object_equal(
            expected=self._expected,
            actual=actual
        )


class NetworkUtilsAddressUndefinedPortTestCaseImpl(TestCase):
    _address: Final[str]

    def __init__(self) -> None:
        self._address = RandomUtils.randtext()

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            NetworkUtils.address(self._address)

        test.assert_exception_equal(
            expected=UserException(
                msg="Invalid address"
            ),
            exception_catcher=exception_catcher
        )


class NetworkUtilsAddressInvalidPortTestCaseImpl(TestCase):
    _address: Final[str]

    def __init__(self) -> None:
        self._address = TextUtils.format_args(
            tpl="{}:",
            args=[
                RandomUtils.randtext()
            ]
        )

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            NetworkUtils.address(self._address)

        test.assert_exception_equal(
            expected=UserException(
                msg="Invalid port"
            ),
            exception_catcher=exception_catcher
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=NetworkUtilsAddressTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=NetworkUtilsAddressUndefinedPortTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=NetworkUtilsAddressInvalidPortTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
