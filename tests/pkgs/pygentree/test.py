from __future__ import annotations

import unittest

from collections.abc import Callable
from dataclasses import dataclass
from typing import Final

from pygencollection.impl import ArrayImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.impl import TreeImpl
from pygentype.abc import Class


@dataclass
class TreeTestCaseMethod(object):
    name: Final[str]
    entrypoint_fn: Final[Callable[[object], object]]
    node_type: Final[Class]


class TreeInvalidNodeTypeTestCaseImpl(TestCase):
    def run(self, test: Test) -> None:
        tree = TreeImpl()

        methods = ArrayImpl.of(
            TreeTestCaseMethod(
                name=tree.create_list_node.__name__,
                entrypoint_fn=tree.create_list_node,
                node_type=list
            ),
            TreeTestCaseMethod(
                name=tree.create_object_node.__name__,
                entrypoint_fn=tree.create_object_node,
                node_type=dict
            )
        )

        for method in methods:
            self._test(test, method, False)
            self._test(test, method, 0.0)
            self._test(test, method, 0)
            self._test(test, method, [])
            self._test(test, method, set[object]())
            self._test(test, method, "")
            self._test(test, method, object())

    def _test(
        self,
        test: Test,
        method: TreeTestCaseMethod,
        node: object
    ) -> None:
        if (method.node_type == node.__class__):
            return

        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="{}: {}",
                args=[
                    method.name,
                    node.__class__.__name__
                ]
            )
        )

        with (sub_test):
            exception_catcher = test.exception_catcher()

            with (exception_catcher):
                method.entrypoint_fn(node)

            expected = TreeInvalidNodeTypeException(
                msg=TextUtils.format_args(
                    tpl="Node [$]: Invalid node type. [{}] instead of [{}]",
                    args=[
                        node.__class__.__name__,
                        method.node_type.__name__
                    ]
                )
            )

            test.assert_exception_equal(
                expected=expected,
                exception_catcher=exception_catcher
            )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=TreeInvalidNodeTypeTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
