from __future__ import annotations

import unittest

from collections.abc import Callable
from typing import Final
from typing import Protocol

from pygenarg.err import ArgEmptyValueException
from pygenargtext.abc import TextArg
from pygenargtext.utils import TextArgUtils
from pygenerr.err import SystemException
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class TextArgTestCaseImpl(TestCase):
    @classmethod
    def create(
        cls,
        name: str,
        description: str,
        value: str,
        arg: TextArg
    ) -> TestCase:
        return cls(
            name=name,
            description=description,
            value=value,
            arg=arg
        )

    @classmethod
    def create_env(
        cls,
        name: str,
        env_name: str,
        value: str,
        arg: TextArg
    ) -> TestCase:
        return cls(
            name=name,
            description=TextUtils.format_args(
                tpl="Env [{}]",
                args=[
                    env_name
                ]
            ),
            value=value,
            arg=arg
        )

    _name: Final[str]
    _description: Final[str]
    _value: Final[str]
    _arg: Final[TextArg]

    def __init__(
        self,
        name: str,
        description: str,
        value: str,
        arg: TextArg
    ) -> None:
        self._name = name
        self._description = description
        self._value = value
        self._arg = arg

    def run(self, test: Test) -> None:
        self._test_value(test)
        self._test_describe(test)

    def _test_describe(self, test: Test) -> None:
        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="{}: {}",
                args=[
                    self._name,
                    TextArg.describe.__name__
                ]
            )
        )

        with (sub_test):
            test.assert_text_equal(
                expected=TextUtils.format_args(
                    tpl="{}: [{}]",
                    args=[
                        self._description,
                        self._value
                    ]
                ),
                actual=self._arg.describe()
            )

    def _test_value(self, test: Test) -> None:
        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="{}: {}",
                args=[
                    self._name,
                    TextArg.value.__name__
                ]
            )
        )

        with (sub_test):
            arg_ctx = self._arg.__enter__()

            test.assert_object_equal(
                expected=self._value,
                actual=arg_ctx.value()
            )


class TextArgEmptyValueTestCaseImpl(TestCase):
    class EntrypointFn(Protocol):
        def __call__(self) -> TextArg:
            raise SystemException()

    @classmethod
    def create(
        cls,
        name: str,
        description: str,
        entrypoint_fn: Callable[[], TextArg]
    ) -> TestCase:
        return cls(
            name=name,
            description=description,
            entrypoint_fn=entrypoint_fn
        )

    @classmethod
    def create_env(
        cls,
        name: str,
        env_name: str,
        entrypoint_fn: Callable[[], TextArg]
    ) -> TestCase:
        return cls(
            name=name,
            description=TextUtils.format_args(
                tpl="Env [{}]",
                args=[
                    env_name
                ]
            ),
            entrypoint_fn=entrypoint_fn
        )

    _name: Final[str]
    _description: Final[str]
    _entrypoint_fn: Final[EntrypointFn]

    def __init__(
        self,
        name: str,
        description: str,
        entrypoint_fn: EntrypointFn
    ) -> None:
        self._name = name
        self._description = description
        self._entrypoint_fn = entrypoint_fn

    def run(self, test: Test) -> None:
        sub_test = test.sub_test(self._name)

        with (sub_test):
            exception_catcher = test.exception_catcher()

            with (exception_catcher):
                self._entrypoint_fn()

            test.assert_exception_equal(
                expected=ArgEmptyValueException(
                    msg=TextUtils.format_args(
                        tpl="[{}]: empty value",
                        args=[
                            self._description
                        ]
                    )
                ),
                exception_catcher=exception_catcher
            )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    text_generator = UniqueRandomTextGeneratorImpl()

    test_suite = UnitTestSuiteImpl()

    value = text_generator.randtext()
    description = text_generator.randtext()
    env_name = text_generator.randtext()
    envs = {
        env_name: value
    }

    test_suite.add_test_case(
        test_case=TextArgTestCaseImpl.create(
            name=TextArgUtils.required.__name__,
            description=description,
            value=value,
            arg=TextArgUtils.required(description, value)
        )
    )
    test_suite.add_test_case(
        test_case=TextArgTestCaseImpl.create(
            name=TextArgUtils.optional.__name__,
            description=description,
            value=value,
            arg=TextArgUtils.optional(description, value)
        )
    )
    test_suite.add_test_case(
        test_case=TextArgTestCaseImpl.create_env(
            name=TextArgUtils.required_env.__name__,
            env_name=env_name,
            value=value,
            arg=TextArgUtils.required_env(envs, env_name)
        )
    )
    test_suite.add_test_case(
        test_case=TextArgTestCaseImpl.create_env(
            name=TextArgUtils.optional_env.__name__,
            env_name=env_name,
            value=value,
            arg=TextArgUtils.optional_env(envs, env_name)
        )
    )
    test_suite.add_test_case(
        test_case=TextArgEmptyValueTestCaseImpl.create(
            name=TextArgUtils.required.__name__,
            description=description,
            entrypoint_fn=lambda: TextArgUtils.required(description, "")
        )
    )
    test_suite.add_test_case(
        test_case=TextArgEmptyValueTestCaseImpl.create_env(
            name=TextArgUtils.required_env.__name__,
            env_name=env_name,
            entrypoint_fn=lambda: TextArgUtils.required_env(
                envs={},
                env_name=env_name
            )
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
