from __future__ import annotations

from typing import Final
from typing import Generic

from pygentype.abc import KT
from pygentype.abc import VT
from pygentype.abc import FinalMapping


class C(
    Generic[
        KT, VT
    ]
):
    v: FinalMapping[
        KT, VT
    ]


def f(
    a0: int, a1: int
) -> object:
    v0 = [
        "", ""
    ]

    v1 = {"": ""}

    return (v0, v1)


FinalC = Final[
    C[
        KT, VT
    ]
]
