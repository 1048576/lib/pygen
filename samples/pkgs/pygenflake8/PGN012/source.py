from __future__ import annotations

from typing import Final

from pygentype.abc import FinalCollection

V: Final[str] = ""


__all__: FinalCollection[str] = [
    "V"
]
