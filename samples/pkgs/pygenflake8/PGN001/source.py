from __future__ import annotations


def f(a: str) -> None:
    ...


if (__name__ == "__main__"):
    f("                                                                      ")
    f("                                                                       ")
