from __future__ import annotations

from collections.abc import Iterator
from typing import Final
from typing import Generic
from typing import TypeVar

from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection

ProcessorT = TypeVar("ProcessorT")
ResultT = TypeVar("ResultT")
ValueT = TypeVar("ValueT")


class TextDocLexer(Generic[ValueT]):
    def tokenize(self, reader: TextReader) -> Iterator[ValueT]:
        raise SystemException()


class TextDocLexerNextFn(Generic[ResultT, ProcessorT]):
    def __call__(self, result: ResultT) -> ProcessorT:
        raise SystemException()


class TextDocLexerProcessFn(Generic[ProcessorT, ResultT]):
    def __call__(
        self,
        processor: ProcessorT,
        reader: TextReader
    ) -> ResultT:
        raise SystemException()


class TextDocLexerSuccessFn(Generic[ResultT]):
    def __call__(self, result: ResultT) -> bool:
        raise SystemException()


class TextDocLexerValueFn(Generic[ResultT, ValueT]):
    def __call__(self, result: ResultT) -> ValueT:
        raise SystemException()


FinalTextDocLexer = Final[TextDocLexer[ValueT]]
FinalTextDocLexerNextFn = Final[TextDocLexerNextFn[ResultT, ProcessorT]]
FinalTextDocLexerProcessFn = Final[TextDocLexerProcessFn[ProcessorT, ResultT]]
FinalTextDocLexerSuccessFn = Final[TextDocLexerSuccessFn[ResultT]]
FinalTextDocLexerValueFn = Final[TextDocLexerValueFn[ResultT, ValueT]]


__all__: FinalCollection[str] = [
    "FinalTextDocLexer",
    "FinalTextDocLexerNextFn",
    "FinalTextDocLexerProcessFn",
    "FinalTextDocLexerSuccessFn",
    "FinalTextDocLexerValueFn",
    "TextDocLexer",
    "TextDocLexerNextFn",
    "TextDocLexerProcessFn",
    "TextDocLexerSuccessFn",
    "TextDocLexerValueFn"
]
