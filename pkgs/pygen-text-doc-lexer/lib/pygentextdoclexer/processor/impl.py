from __future__ import annotations

from collections.abc import Callable
from collections.abc import Iterable
from typing import Final
from typing import Generic
from typing import TypeVar

from pygencollection.impl import ArrayImpl
from pygenerr.err import SystemException
from pygentextdoclexer.abc import FinalTextDocLexerProcessFn
from pygentextdoclexer.abc import FinalTextDocLexerSuccessFn
from pygentextdoclexer.abc import TextDocLexerProcessFn
from pygentextdoclexer.abc import TextDocLexerSuccessFn
from pygentextreader.abc import TextReader
from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection

ProcessorT = TypeVar("ProcessorT")
ResultT = TypeVar("ResultT")


class TextDocLexerCacheProcessorImpl(Generic[ResultT]):
    _result: Final[ResultT]

    def __init__(self, result: ResultT) -> None:
        self._result = result

    def process(self, reader: TextReader) -> ResultT:
        return self._result


class TextDocLexerForkProcessorImpl(Generic[ProcessorT, ResultT]):
    _processors: FinalCollection[ProcessorT]
    _on_failure_fn: Final[Callable[[], ResultT]]
    _process_fn: FinalTextDocLexerProcessFn[ProcessorT, ResultT]
    _success_fn: FinalTextDocLexerSuccessFn[ResultT]

    def __init__(
        self,
        processors: Iterable[ProcessorT],
        on_failure_fn: Callable[[], ResultT],
        process_fn: TextDocLexerProcessFn[ProcessorT, ResultT],
        success_fn: TextDocLexerSuccessFn[ResultT]
    ) -> None:
        self._processors = ArrayImpl.copy(processors)
        self._on_failure_fn = on_failure_fn
        self._process_fn = process_fn
        self._success_fn = success_fn

    def process(self, reader: TextReader) -> ResultT:
        for processor in self._processors:
            result = self._process_fn(processor, reader)

            if (self._success_fn(result)):
                return result

        return self._on_failure_fn()


class TextDocLexerLnProcessorImpl(Generic[ResultT]):
    _on_success_fn: Final[Callable[[], ResultT]]
    _on_failure_fn: Final[Callable[[TextReaderPosition], ResultT]]

    def __init__(
        self,
        on_success_fn: Callable[[], ResultT],
        on_failure_fn: Callable[[TextReaderPosition], ResultT]
    ) -> None:
        self._on_success_fn = on_success_fn
        self._on_failure_fn = on_failure_fn

    def process(self, reader: TextReader) -> ResultT:
        if (reader.peek(0) == "\n"):
            reader.forward(1)

            return self._on_success_fn()
        else:
            return self._on_failure_fn(reader.position())


class TextDocLexerStubProcessorImpl(Generic[ResultT]):
    def process(self, reader: TextReader) -> ResultT:
        raise SystemException()


__all__: FinalCollection[str] = [
    "TextDocLexerCacheProcessorImpl",
    "TextDocLexerForkProcessorImpl",
    "TextDocLexerLnProcessorImpl",
    "TextDocLexerStubProcessorImpl"
]
