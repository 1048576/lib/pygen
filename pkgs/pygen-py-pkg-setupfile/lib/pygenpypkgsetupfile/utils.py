from __future__ import annotations

from pygenpath.abc import Filepath
from pygenpypkgsetupfile.abc import PyPkgSetupfile
from pygenpypkgsetupfile.impl import PyPkgSetupfileParserImpl
from pygentype.abc import FinalCollection


class PyPkgSetupfileUtils(object):
    @classmethod
    def parse(cls, filepath: Filepath) -> PyPkgSetupfile:
        parser = PyPkgSetupfileParserImpl(filepath)

        return parser.parse()


__all__: FinalCollection[str] = [
    "PyPkgSetupfileUtils"
]
