from __future__ import annotations

from collections.abc import Collection
from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping


@dataclass
class PyPkgSetupfile(object):
    name: Final[str]
    package_data: FinalMapping[str, Collection[str]]
    package_dir: FinalMapping[str, str]
    entry_points: FinalMapping[str, Collection[str]]
    scripts: FinalCollection[str]


class PyPkgSetupfileParser(object):
    def parse(self) -> PyPkgSetupfile:
        raise SystemException()


__all__: FinalCollection[str] = [
    "PyPkgSetupfile",
    "PyPkgSetupfileParser"
]
