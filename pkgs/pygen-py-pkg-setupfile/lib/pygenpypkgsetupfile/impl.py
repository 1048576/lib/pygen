from __future__ import annotations

from typing import Final

from pygenast.abc import ASTCallExpr
from pygenast.abc import ASTExprStmt
from pygenast.abc import ASTIfStmt
from pygenast.abc import ASTModule
from pygenast.utils import ASTUtils
from pygenastanalyzer.abc import ASTAnalyzer
from pygenastanalyzer.impl import ASTAnalyzerImpl
from pygenastanalyzer.impl import ASTDictProcessorImpl
from pygenastanalyzer.impl import ASTListProcessorImpl
from pygenastanalyzer.impl import ASTTextConstantProcessorImpl
from pygencollection.impl import ArrayImpl
from pygenpath.abc import Filepath
from pygenpypkgsetupfile.abc import PyPkgSetupfile
from pygenpypkgsetupfile.abc import PyPkgSetupfileParser
from pygenpypkgsetupfile.err import PyPkgSetupfileException
from pygentype.abc import FinalCollection


class PyPkgSetupfileParserImpl(PyPkgSetupfileParser):
    METHOD_NAME: Final[str] = "setuptools.setup"

    _filepath: Final[Filepath]
    _analyzer: Final[ASTAnalyzer]

    def __init__(self, filepath: Filepath) -> None:
        self._filepath = filepath
        self._analyzer = ASTAnalyzerImpl(
            name="Setupfile",
            filepath=filepath
        )

    def parse(self) -> PyPkgSetupfile:
        source = self._read_setupfile()

        tree = self._parse_setupfile(source)

        setup_method_call = self._find_setup_method_call(tree)

        name = self._analyzer.read_call_argument(
            what=self.METHOD_NAME,
            expr=setup_method_call,
            name="name",
            processor=ASTTextConstantProcessorImpl()
        )

        package_data = self._analyzer.read_call_argument(
            what=self.METHOD_NAME,
            expr=setup_method_call,
            name="package_data",
            processor=ASTDictProcessorImpl(
                fn=ASTListProcessorImpl(
                    fn=ASTTextConstantProcessorImpl()
                )
            )
        )

        package_dir = self._analyzer.read_call_argument(
            what=self.METHOD_NAME,
            expr=setup_method_call,
            name="package_dir",
            processor=ASTDictProcessorImpl(
                fn=ASTTextConstantProcessorImpl()
            )
        )

        entry_points = self._analyzer.read_call_argument(
            what=self.METHOD_NAME,
            expr=setup_method_call,
            name="entry_points",
            processor=ASTDictProcessorImpl(
                fn=ASTListProcessorImpl(
                    fn=ASTTextConstantProcessorImpl()
                )
            )
        )

        scripts = ArrayImpl.copy(
            instance=self._analyzer.read_call_argument(
                what=self.METHOD_NAME,
                expr=setup_method_call,
                name="scripts",
                processor=ASTListProcessorImpl(
                    fn=ASTTextConstantProcessorImpl()
                )
            )
        )

        return PyPkgSetupfile(
            name=name,
            package_data=package_data,
            package_dir=package_dir,
            entry_points=entry_points,
            scripts=scripts
        )

    def _find_setup_method_call(self, tree: ASTModule) -> ASTCallExpr:
        for if_stmt in ASTUtils.filter(tree.body, ASTIfStmt):
            for expr_stmt in ASTUtils.filter(if_stmt.body, ASTExprStmt):
                if (isinstance(expr_stmt.value, ASTCallExpr)):
                    call_expr = expr_stmt.value
                else:
                    continue

                if (ASTUtils.name_equal(call_expr.func, self.METHOD_NAME)):
                    return call_expr

        raise PyPkgSetupfileException.setup_method_call_not_found(
            filepath=self._filepath
        )

    def _parse_setupfile(self, source: str) -> ASTModule:
        try:
            return ASTUtils.parse(source, self._filepath.path())
        except SyntaxError as e:
            raise PyPkgSetupfileException.could_not_be_parsed(
                filepath=self._filepath,
                e=e
            )

    def _read_setupfile(self) -> str:
        try:
            with (open(self._filepath.path()) as f):
                return f.read()
        except OSError as e:
            raise PyPkgSetupfileException.could_not_be_read(self._filepath, e)


__all__: FinalCollection[str] = [
    "PyPkgSetupfileParserImpl"
]
