from __future__ import annotations

from collections.abc import Iterable
from typing import Final

import netaddr.ip.sets

from pygenerr.err import SystemException
from pygennetaddr.abc import IPSet
from pygentype.abc import FinalCollection


class IPSetImpl(IPSet):
    @classmethod
    def create(cls, addresses: Iterable[str]) -> IPSet:
        return cls(
            ip_set=netaddr.ip.sets.IPSet(addresses)
        )

    _ip_set: Final[netaddr.ip.sets.IPSet]

    def __init__(
        self,
        ip_set: netaddr.ip.sets.IPSet
    ) -> None:
        self._ip_set = ip_set

    def __str__(self) -> str:
        return str(self._ip_set)

    def intersection(self, other: IPSet) -> IPSet:
        if (not isinstance(other, IPSetImpl)):
            raise SystemException()

        return IPSetImpl(
            ip_set=self._ip_set.intersection(other._ip_set)
        )

    def union(self, other: IPSet) -> IPSet:
        if (not isinstance(other, IPSetImpl)):
            raise SystemException()

        return IPSetImpl(
            ip_set=self._ip_set.union(other._ip_set)
        )


__all__: FinalCollection[str] = [
    "IPSetImpl"
]
