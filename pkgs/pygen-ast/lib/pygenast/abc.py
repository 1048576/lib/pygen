from __future__ import annotations

import ast

from typing import TypeVar

from pygentype.abc import FinalCollection

ASTNode = ast.AST
ASTNodeT = TypeVar(
    name="ASTNodeT",
    bound=ASTNode
)


ASTModule = ast.Module


ASTAttributeExpr = ast.Attribute
ASTBinOp = ast.BinOp
ASTCallExpr = ast.Call
ASTCompare = ast.Compare
ASTConstantExpr = ast.Constant
ASTDictExpr = ast.Dict
ASTGeneratorExp = ast.GeneratorExp
ASTLambdaExpr = ast.Lambda
ASTListExpr = ast.List
ASTNameExpr = ast.Name
ASTStarred = ast.Starred
ASTSubscriptExpr = ast.Subscript
ASTTupleExpr = ast.Tuple
ASTUnaryOp = ast.UnaryOp
ASTYieldExpr = ast.Yield


ASTAnnAssignStmt = ast.AnnAssign
ASTAssertStmt = ast.Assert
ASTAssignStmt = ast.Assign
ASTAsyncFunctionDefStmt = ast.AsyncFunctionDef
ASTAugAssignStmt = ast.AugAssign
ASTBreakStmt = ast.Break
ASTClassDefStmt = ast.ClassDef
ASTContinueStmt = ast.Continue
ASTDeleteStmt = ast.Delete
ASTExprStmt = ast.Expr
ASTForStmt = ast.For
ASTFunctionDefStmt = ast.FunctionDef
ASTGlobalStmt = ast.Global
ASTIfStmt = ast.If
ASTImportFromStmt = ast.ImportFrom
ASTImportStmt = ast.Import
ASTNonlocalStmt = ast.Nonlocal
ASTPassStmt = ast.Pass
ASTRaiseStmt = ast.Raise
ASTReturnStmt = ast.Return
ASTTryStmt = ast.Try
ASTWhileStmt = ast.While
ASTWithStmt = ast.With


__all__: FinalCollection[str] = [
    "ASTAnnAssignStmt",
    "ASTAssertStmt",
    "ASTAssignStmt",
    "ASTAsyncFunctionDefStmt",
    "ASTAttributeExpr",
    "ASTAugAssignStmt",
    "ASTBinOp",
    "ASTBreakStmt",
    "ASTCallExpr",
    "ASTClassDefStmt",
    "ASTCompare",
    "ASTConstantExpr",
    "ASTContinueStmt",
    "ASTDeleteStmt",
    "ASTDictExpr",
    "ASTExprStmt",
    "ASTForStmt",
    "ASTFunctionDefStmt",
    "ASTGeneratorExp",
    "ASTGlobalStmt",
    "ASTIfStmt",
    "ASTImportFromStmt",
    "ASTImportStmt",
    "ASTLambdaExpr",
    "ASTListExpr",
    "ASTModule",
    "ASTNameExpr",
    "ASTNode",
    "ASTNodeT",
    "ASTNonlocalStmt",
    "ASTPassStmt",
    "ASTRaiseStmt",
    "ASTReturnStmt",
    "ASTStarred",
    "ASTSubscriptExpr",
    "ASTTryStmt",
    "ASTTupleExpr",
    "ASTUnaryOp",
    "ASTWhileStmt",
    "ASTWithStmt",
    "ASTYieldExpr"
]
