from __future__ import annotations

from typing import Final

from pygenargnet.abc import NetworkAddressArg
from pygenargnet.abc import NetworkAddressArgContext
from pygenerr.err import UserException
from pygennet.abc import NetworkAddress
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class NetworkAddressArgImpl(NetworkAddressArg, NetworkAddressArgContext):
    _description: Final[str]
    _value: Final[NetworkAddress]

    def __init__(
        self,
        description: str,
        value: NetworkAddress
    ) -> None:
        self._description = description
        self._value = value

    def __enter__(self) -> NetworkAddressArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title=self.describe()
            )

    def describe(self) -> str:
        return TextUtils.format_args(
            tpl="{}: [{}]",
            args=[
                self._description,
                self._value
            ]
        )

    def value(self) -> NetworkAddress:
        return self._value


__all__: FinalCollection[str] = [
    "NetworkAddressArgImpl"
]
