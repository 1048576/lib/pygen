from __future__ import annotations

from typing import TypeVar

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class Path(object):
    def path(self) -> str:
        raise SystemException()


class Filepath(Path):
    ...


class Dirpath(Path):
    def contains(self, path: Path) -> bool:
        raise SystemException()

    def resolve_dirpath(self, path: str) -> Dirpath:
        raise SystemException()

    def resolve_filepath(self, path: str) -> Filepath:
        raise SystemException()


class PathMapper(object):
    def dirpath(self, path: str) -> Dirpath:
        raise SystemException()

    def filename(self, path: str) -> str:
        raise SystemException()

    def filepath(self, path: str) -> Filepath:
        raise SystemException()

    def normalize(self, path: str) -> str:
        raise SystemException()

    def raise_exception_on_unnormalized_dir_path(self, path: str) -> None:
        raise SystemException()

    def raise_exception_on_unnormalized_file_path(self, path: str) -> None:
        raise SystemException()

    def raise_exception_on_unnormalized_path(self, path: str) -> None:
        raise SystemException()


PathT = TypeVar("PathT", Dirpath, Filepath)


__all__: FinalCollection[str] = [
    "Dirpath",
    "Filepath",
    "Path",
    "PathMapper",
    "PathT"
]
