from __future__ import annotations

from collections.abc import Callable
from collections.abc import Collection
from collections.abc import Iterable
from collections.abc import Iterator
from collections.abc import Mapping
from typing import Any
from typing import Final
from typing import Generic
from typing import NoReturn
from typing import Self

from pygencollection.abc import Array
from pygencollection.abc import Dict
from pygencollection.abc import FrozenSet
from pygencollection.abc import HasIterator
from pygencollection.abc import IterableInstance
from pygencollection.abc import IteratorInstance
from pygencollection.abc import List
from pygencollection.abc import NamedPair
from pygenerr.err import SystemException
from pygentype.abc import KT
from pygentype.abc import VT
from pygentype.abc import FinalCollection
from pygentype.abc import Mutable
from pygentype.abc import Optional
from pygentype.abc import Pair
from pygentype.abc import T


class NamedPairImpl(Generic[KT, VT], NamedPair[KT, VT]):
    def args(self) -> Pair[KT, VT]:
        return (self.key, self.value)


class CollectionEncoderImpl(object):
    def encode(self, instance: object) -> object:
        if (isinstance(instance, HasIterator)):
            return list(iter(instance))
        else:
            raise SystemException()

    def support(self, instance: object) -> bool:
        return isinstance(instance, HasIterator)


class DictImpl(Generic[KT, VT], dict[KT, VT], Dict[KT, VT]):
    @classmethod
    def as_mapping(cls, pairs: Mapping[KT, VT]) -> Self:
        return cls(pairs)

    def detach(self, index: KT) -> Optional[VT]:
        return super().pop(index, None)

    def pop(*args: Any, **kwargs: Any) -> VT:
        raise SystemException()


class EmptyIteratorImpl(IteratorInstance):
    def __next__(self) -> NoReturn:
        raise StopIteration()


class ArrayImpl(Generic[T], Array[T], Collection[T], HasIterator):
    @classmethod
    def collect(cls, iterator: Iterator[T]) -> Self:
        return cls(tuple(iterator))

    @classmethod
    def copy(cls, instance: Iterable[T]) -> Self:
        if (isinstance(instance, cls)):
            return instance
        else:
            return cls(
                items=tuple(instance)
            )

    @classmethod
    def of(cls, *items: T) -> Self:
        return cls(
            items=tuple(items)
        )

    _contains_fn: Final[Callable[[object], bool]]
    _getitem_fn: Final[Callable[[int], T]]
    _iter_fn: Final[Callable[[], Iterator[T]]]
    _len_fn: Final[Callable[[], int]]

    def __init__(self, items: tuple[T, ...] = tuple()) -> None:
        self._contains_fn = items.__contains__
        self._getitem_fn = items.__getitem__
        self._iter_fn = items.__iter__
        self._len_fn = items.__len__

    def __contains__(self, item: object) -> bool:
        return self._contains_fn(item)

    def __getitem__(self, index: int) -> T:
        return self._getitem_fn(index)

    def __iter__(self) -> Iterator[T]:
        return self._iter_fn()

    def __len__(self) -> int:
        return self._len_fn()

    def __repr__(self) -> str:
        return repr(list(self))


class FrozenSetImpl(Generic[T], FrozenSet[T], HasIterator):
    @classmethod
    def copy(cls, instance: Iterable[T]) -> Self:
        if (isinstance(instance, cls)):
            return instance
        else:
            return cls(
                items=frozenset(instance)
            )

    _contains_fn: Final[Callable[[object], bool]]
    _iter_fn: Final[Callable[[], Iterator[T]]]
    _len_fn: Final[Callable[[], int]]

    def __init__(self, items: frozenset[T] = frozenset()) -> None:
        self._contains_fn = items.__contains__
        self._iter_fn = items.__iter__
        self._len_fn = items.__len__

    def __contains__(self, item: object) -> bool:
        return self._contains_fn(item)

    def __iter__(self) -> Iterator[T]:
        return self._iter_fn()

    def __len__(self) -> int:
        return self._len_fn()


class FrozenStrListImpl(ArrayImpl[str]):
    ...


class FrozenStrSetImpl(FrozenSetImpl[str]):
    ...


class ListImpl(Generic[T], List[T], HasIterator):
    @classmethod
    def of(cls, *items: T) -> Self:
        return cls(iter(items))

    _contains_fn: Final[Callable[[object], bool]]
    _getitem_fn: Final[Callable[[int], T]]
    _iter_fn: Final[Callable[[], Iterator[T]]]
    _len_fn: Final[Callable[[], int]]
    _add_fn: Final[Callable[[T], None]]
    _detach_fn: Final[Callable[[int], T]]

    def __init__(self, iterator: Iterator[T] = iter([])) -> None:
        items = list(iterator)

        self._contains_fn = items.__contains__
        self._getitem_fn = items.__getitem__
        self._iter_fn = items.__iter__
        self._len_fn = items.__len__
        self._add_fn = items.append
        self._detach_fn = items.pop

    def __contains__(self, item: object) -> bool:
        return self._contains_fn(item)

    def __getitem__(self, index: int) -> T:
        return self._getitem_fn(index)

    def __iter__(self) -> Iterator[T]:
        return self._iter_fn()

    def __len__(self) -> int:
        return self._len_fn()

    def add(self, value: T) -> None:
        return self._add_fn(value)

    def detach(self, index: int) -> T:
        return self._detach_fn(index)


class OnceIterableInstanceImpl(Generic[T], IterableInstance[T]):
    _iter_fn: Mutable[Callable[[], Iterator[T]]]

    def __init__(self, iterator: Iterator[T]) -> None:
        self._iter_fn = lambda: iterator

    def __iter__(self) -> Iterator[T]:
        iterator = self._iter_fn()

        self._iter_fn = self._raise_exception

        return iterator

    def _raise_exception(self) -> Iterator[T]:
        raise SystemException()


class StrListImpl(ListImpl[str]):
    ...


__all__: FinalCollection[str] = [
    "ArrayImpl",
    "CollectionEncoderImpl",
    "DictImpl",
    "EmptyIteratorImpl",
    "FrozenSetImpl",
    "FrozenStrListImpl",
    "FrozenStrSetImpl",
    "ListImpl",
    "NamedPairImpl",
    "OnceIterableInstanceImpl",
    "StrListImpl"
]
