from __future__ import annotations

import abc

from collections.abc import Collection
from collections.abc import Iterable
from collections.abc import Iterator
from collections.abc import Mapping
from collections.abc import Sequence
from dataclasses import dataclass
from typing import Any
from typing import Final
from typing import Generic
from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import KT
from pygentype.abc import VT
from pygentype.abc import CovariantT
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygentype.abc import Pair
from pygentype.abc import T


class Array(Protocol[CovariantT]):
    def __contains__(self, item: object, /) -> bool:
        raise SystemException()

    def __eq__(self, __value: object) -> bool:
        raise SystemException()

    def __getitem__(self, index: int, /) -> CovariantT:
        raise SystemException()

    def __iter__(self) -> Iterator[CovariantT]:
        raise SystemException()

    def __len__(self) -> int:
        raise SystemException()


class Dict(Generic[KT, VT], Mapping[KT, VT]):
    def __setitem__(self, index: KT, value: VT) -> None:
        raise SystemException()

    def detach(self, index: KT) -> Optional[VT]:
        raise SystemException()


class FrozenSet(Generic[T], Collection[T]):
    def __contains__(self, item: object) -> bool:
        raise SystemException()

    def __eq__(self, __value: object) -> bool:
        raise SystemException()


class List(Generic[T], Array[T]):
    def add(self, value: T) -> None:
        raise SystemException()

    def detach(self, index: int) -> T:
        raise SystemException()


@dataclass
class NamedPair(Generic[KT, VT]):
    key: Final[KT]
    value: Final[VT]

    def args(self) -> Pair[KT, VT]:
        raise SystemException()


class HasIterator(Iterable[Any]):
    ...


class IteratorInstance(Iterator[Any]):
    ...


class IterableInstance(Generic[T]):
    def __iter__(self) -> Iterator[T]:
        raise SystemException()


class ArrayInstance(Array[Any]):
    ...


class SequenceInstanceMeta(abc.ABCMeta):
    def __subclasscheck__(cls, subclass: object) -> bool:
        if (subclass is list):
            return True

        return (subclass is cls)


class SequenceInstance(Sequence[Any], metaclass=SequenceInstanceMeta):
    ...


class MappingInstanceMeta(abc.ABCMeta):
    def __subclasscheck__(cls, subclass: object) -> bool:
        if (subclass is dict):
            return True

        return (subclass is cls)


class MappingInstance(Mapping[Any, Any], metaclass=MappingInstanceMeta):
    ...


FinalArray = Final[Array[T]]
FinalDict = Final[Dict[KT, VT]]
FinalList = Final[List[T]]


__all__: FinalCollection[str] = [
    "Array",
    "ArrayInstance",
    "Dict",
    "FinalArray",
    "FinalDict",
    "FinalList",
    "FrozenSet",
    "HasIterator",
    "IterableInstance",
    "IteratorInstance",
    "List",
    "MappingInstance",
    "NamedPair",
    "SequenceInstance"
]
