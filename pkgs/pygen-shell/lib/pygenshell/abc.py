from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import Final

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import Pair


class ShellOutputSource(Enum):
    STDOUT = "STDOUT"
    STDERR = "STDERR"


@dataclass
class ShellOutputEntry(object):
    source: Final[ShellOutputSource]
    data: Final[str]


class ShellOutput(object):
    def write(self, entry: ShellOutputEntry) -> None:
        raise SystemException()


class ShellSocket(object):
    class Marker(Enum):
        ARG = b"a"
        RUN = b"r"
        STDOUT = b"o"
        STDERR = b"e"
        STATUS = b"s"

    def receive(self) -> Pair[Marker, str]:
        raise SystemException()

    def send(self, marker: Marker, data: str) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "ShellOutput",
    "ShellOutputEntry",
    "ShellOutputSource",
    "ShellSocket"
]
