from __future__ import annotations

from typing import Final

from pygenshell.abc import ShellSocket
from pygensocket.abc import Socket
from pygentype.abc import FinalCollection
from pygentype.abc import Pair


class ShellSocketImpl(ShellSocket):
    MARKER_LENGTH: Final[int] = 1
    DATA_SIZE_LENGTH: Final[int] = 4

    _socket: Final[Socket]

    def __init__(self, socket: Socket) -> None:
        self._socket = socket

    def receive(self) -> Pair[ShellSocket.Marker, str]:
        marker = self._socket.receive(self.MARKER_LENGTH)
        data_size_as_bytes = self._socket.receive(self.DATA_SIZE_LENGTH)
        data_size = int.from_bytes(data_size_as_bytes)

        if (data_size > 0):
            data_as_bytes = self._socket.receive(data_size)
            data = data_as_bytes.decode()
        else:
            data = ""

        return (ShellSocket.Marker(marker), data)

    def send(self, marker: ShellSocket.Marker, data: str) -> None:
        data_as_bytes = data.encode()
        data_size = len(data_as_bytes)
        data_size_as_bytes = data_size.to_bytes(self.DATA_SIZE_LENGTH)

        self._socket.send(marker.value)
        self._socket.send(data_size_as_bytes)

        if (data_size > 0):
            self._socket.send(data_as_bytes)


__all__: FinalCollection[str] = [
    "ShellSocketImpl"
]
