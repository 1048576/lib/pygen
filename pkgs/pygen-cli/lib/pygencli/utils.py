from __future__ import annotations

from collections.abc import Mapping
from typing import Final

from pygencli.abc import CliArg
from pygencli.abc import CliArgValueContextManager
from pygencli.abc import CliCmd
from pygencli.abc import CliParser
from pygencli.abc import CliParserBuilder
from pygencli.argvalue.impl import CliArgValueContextManagerImpl
from pygencli.parser.impl import CliParserBuilderImpl


class CliUtils(object):
    _PARSER_BUILDER: Final[CliParserBuilder] = CliParserBuilderImpl()

    @classmethod
    def parser(cls, command: CliCmd) -> CliParser:
        return cls._PARSER_BUILDER.build(command)

    @classmethod
    def value(
        cls,
        arg: CliArg,
        args: Mapping[str, str]
    ) -> CliArgValueContextManager:
        return CliArgValueContextManagerImpl(
            metavar=arg.metavar,
            args=args
        )
