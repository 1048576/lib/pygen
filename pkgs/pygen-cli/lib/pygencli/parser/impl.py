from __future__ import annotations

from argparse import ArgumentParser
from collections.abc import Iterable
from collections.abc import Iterator
from collections.abc import Mapping
from collections.abc import Sequence
from typing import Final

from pygencli.abc import CliCmd
from pygencli.abc import CliParser
from pygencli.abc import CliParserBuilder
from pygencli.abc import CliRootSimpleCmd
from pygencli.abc import CliSimpleCmd
from pygencli.err import CliException
from pygencollection.impl import FrozenSetImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class CliParserImpl(CliParser):
    _parser: Final[ArgumentParser]
    _flags: FinalCollection[str]

    def __init__(
        self,
        parser: ArgumentParser,
        flags: Iterable[str]
    ) -> None:
        self._parser = parser
        self._flags = FrozenSetImpl.copy(flags)

    def parse(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> Mapping[str, str]:
        parsed_args = vars(self._parser.parse_args(args))

        for key, value in parsed_args.items():
            if (key not in self._flags):
                continue
            elif (value is None):
                parsed_args[key] = envs.get(key)
            elif (key in envs):
                raise CliException(
                    msg=TextUtils.format_args(
                        tpl=(
                            "Environment variable [{}] "
                            "conflicts with corresponding "
                            "command-line argument"
                        ),
                        args=[
                            key
                        ]
                    )
                )

        return parsed_args


class CliParserBuilderImpl(CliParserBuilder):
    def build(self, command: CliCmd) -> CliParser:
        parser = ArgumentParser()

        flags = self._add_command(parser, command)

        return CliParserImpl(
            parser=parser,
            flags=FrozenSetImpl.copy(flags)
        )

    def _add_command(
        self,
        parser: ArgumentParser,
        command: CliCmd
    ) -> Iterator[str]:
        for option in command.options:
            name = TextUtils.format_args(
                tpl="--{}",
                args=[
                    option.name
                ]
            )
            dest = option.arg.metavar

            parser.add_argument(name, dest=dest)  # noqa: PGN000

            yield option.arg.metavar

        if (isinstance(command, CliSimpleCmd)):
            ...
        elif (isinstance(command, CliRootSimpleCmd)):
            ...
        else:
            subparsers = parser.add_subparsers(
                dest=command.arg.metavar,
                metavar=command.arg.metavar
            )

            for subcommand in command.commands:
                flags = self._add_command(
                    parser=subparsers.add_parser(
                        name=subcommand.name,
                        help=subcommand.help
                    ),
                    command=subcommand
                )

                for flag in flags:
                    yield flag


__all__: FinalCollection[str] = [
    "CliParserBuilderImpl"
]
