from __future__ import annotations

from collections.abc import Iterable

from pygenerr.err import SystemException
from pygenshell.abc import ShellOutput
from pygentype.abc import FinalCollection


class ShellServerProcessor(object):
    def process(
        self,
        args: Iterable[str],
        output: ShellOutput
    ) -> int:
        raise SystemException()


class ShellServer(object):
    def serve(self) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "ShellServer",
    "ShellServerProcessor"
]
