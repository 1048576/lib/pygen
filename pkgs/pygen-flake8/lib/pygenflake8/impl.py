from __future__ import annotations

import dataclasses
import importlib
import typing

from collections.abc import Callable
from collections.abc import Iterable
from collections.abc import Iterator
from typing import Final
from typing import Protocol

from pygenast.abc import ASTAnnAssignStmt
from pygenast.abc import ASTAssertStmt
from pygenast.abc import ASTAssignStmt
from pygenast.abc import ASTAsyncFunctionDefStmt
from pygenast.abc import ASTAttributeExpr
from pygenast.abc import ASTAugAssignStmt
from pygenast.abc import ASTBinOp
from pygenast.abc import ASTBreakStmt
from pygenast.abc import ASTCallExpr
from pygenast.abc import ASTClassDefStmt
from pygenast.abc import ASTCompare
from pygenast.abc import ASTConstantExpr
from pygenast.abc import ASTContinueStmt
from pygenast.abc import ASTDeleteStmt
from pygenast.abc import ASTDictExpr
from pygenast.abc import ASTExprStmt
from pygenast.abc import ASTForStmt
from pygenast.abc import ASTFunctionDefStmt
from pygenast.abc import ASTGeneratorExp
from pygenast.abc import ASTGlobalStmt
from pygenast.abc import ASTIfStmt
from pygenast.abc import ASTImportFromStmt
from pygenast.abc import ASTImportStmt
from pygenast.abc import ASTLambdaExpr
from pygenast.abc import ASTListExpr
from pygenast.abc import ASTModule
from pygenast.abc import ASTNameExpr
from pygenast.abc import ASTNode
from pygenast.abc import ASTNodeT
from pygenast.abc import ASTNonlocalStmt
from pygenast.abc import ASTPassStmt
from pygenast.abc import ASTRaiseStmt
from pygenast.abc import ASTReturnStmt
from pygenast.abc import ASTStarred
from pygenast.abc import ASTSubscriptExpr
from pygenast.abc import ASTTryStmt
from pygenast.abc import ASTTupleExpr
from pygenast.abc import ASTUnaryOp
from pygenast.abc import ASTWhileStmt
from pygenast.abc import ASTWithStmt
from pygenast.abc import ASTYieldExpr
from pygenast.utils import ASTUtils
from pygencollection.abc import Array
from pygencollection.abc import FinalArray
from pygencollection.impl import ArrayImpl
from pygencollection.impl import DictImpl
from pygencollection.impl import EmptyIteratorImpl
from pygencollection.impl import StrListImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import SystemException
from pygenflake8.abc import Flake8Checker
from pygenflake8.abc import Flake8CheckerCache
from pygenflake8.abc import Flake8CheckerModule
from pygenflake8.abc import Flake8Report
from pygenflake8.abc import Flake8ReportEntry
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Type


class Flake8Reports(Protocol):
    def __iter__(self) -> Iterator[Flake8Report]:
        raise SystemException()


class Flake8ReportEntryImpl(Flake8ReportEntry):
    def __iter__(self) -> Iterator[object]:
        yield self.line
        yield self.column
        yield self.text
        yield None


class Flake8CheckerImpl(Flake8Checker):
    MAX_LINE_LENGTH: Final[int] = 79
    TAB_LENGTH: Final[int] = 4

    _module: Final[ASTModule]
    _lines: FinalArray[str]
    _filename: Final[str]

    def __init__(
        self,
        tree: ASTModule,
        filename: str,
        lines: Array[str]
    ) -> None:
        self._module = tree
        self._lines = lines
        self._filename = filename

    def run(self) -> Flake8Report:
        cache = Flake8CheckerCache(
            modules=DictImpl.as_mapping(
                pairs={
                    typing.__name__: Flake8CheckerModule(
                        exported_symbols=ArrayImpl.of(
                            "Annotated",
                            "Any",
                            "Final",
                            "Generic",
                            "IO",
                            "NoReturn",
                            "Protocol",
                            "Self",
                            "TypeVar"
                        )
                    ),
                    dataclasses.__name__: Flake8CheckerModule(
                        exported_symbols=ArrayImpl.of(
                            dataclasses.dataclass.__name__
                        )
                    )
                }
            )
        )

        return self._union_reports(
            reports=[
                self._check_line_length(),
                self._find_and_check(
                    nodes=self._module.body,
                    t=ASTAnnAssignStmt,
                    fn=self._check_module_variable
                ),
                self._check_body(
                    cache=cache,
                    owner=self._module,
                    body=self._module.body
                ),
                self._check_future_annotations(
                    module=self._module
                )
            ]
        )

    def _check_base_classes(
        self,
        class_def_stmt: ASTClassDefStmt
    ) -> Flake8Report:
        if (not self._filename.endswith(".pyi")):
            if (not any(class_def_stmt.bases)):
                yield Flake8ReportEntryImpl(
                    line=class_def_stmt.lineno,
                    column=class_def_stmt.col_offset,
                    text="PGN002 Specify base class"
                )

    def _check_body(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        body: Array[ASTNode]
    ) -> Flake8Report:
        return self._union_reports(
            reports=self._process_body(
                cache=cache,
                owner=owner,
                body=body
            )
        )

    def _check_call_args(
        self,
        call_expr: ASTCallExpr
    ) -> Flake8Report:
        if (any(call_expr.keywords)):
            is_named_keyword_used = CollectionUtils.any(
                items=call_expr.keywords,
                fn=lambda keyword: (keyword.arg is not None)
            )

            if (is_named_keyword_used):
                return Flake8ReportEntryImpl(
                    line=call_expr.lineno,
                    column=call_expr.col_offset,
                    text="PGN000 Use either args or kwargs"
                )

        if (call_expr.args[-1].lineno != call_expr.lineno):
            if (isinstance(call_expr.func, ASTAttributeExpr)):
                name = call_expr.func.attr
            else:
                name = ""

            if (name != "of"):
                yield Flake8ReportEntryImpl(
                    line=call_expr.lineno,
                    column=call_expr.col_offset,
                    text="PGN000 Use kwargs instead of args"
                )

    def _check_class_function_def_order(
        self,
        class_def_stmt: ASTClassDefStmt
    ) -> Flake8Report:
        stmts = ArrayImpl.collect(
            iterator=ASTUtils.filter(
                nodes=class_def_stmt.body,
                t=ASTFunctionDefStmt
            )
        )

        private_classmethod_names = StrListImpl()
        public_classmethod_names = StrListImpl()
        init_names = StrListImpl()
        builtin_names = StrListImpl()
        private_names = StrListImpl()
        public_names = StrListImpl()

        for stmt in stmts:
            name = stmt.name

            is_classmethod = CollectionUtils.any(
                items=ASTUtils.filter(
                    nodes=stmt.decorator_list,
                    t=ASTNameExpr
                ),
                fn=lambda name: (name.id == "classmethod")
            )

            if (is_classmethod):
                if (name.startswith("_")):
                    private_classmethod_names.add(name)
                else:
                    public_classmethod_names.add(name)
            else:
                if (name == "__init__"):
                    init_names.add(name)
                elif (name.startswith("__") and name.endswith("__")):
                    builtin_names.add(name)
                elif (name.startswith("_")):
                    private_names.add(name)
                else:
                    public_names.add(name)

        ordered_names = CollectionUtils.chain(
            links=[
                sorted(public_classmethod_names),
                sorted(private_classmethod_names),
                init_names,
                sorted(builtin_names),
                sorted(public_names),
                sorted(private_names)
            ]
        )

        for stmt, name in CollectionUtils.join_by_index(stmts, ordered_names):
            if (stmt.name != name):
                yield Flake8ReportEntryImpl(
                    line=stmt.lineno,
                    column=stmt.col_offset,
                    text=TextUtils.format_args(
                        tpl="PGN014 Move function def [{}] here",
                        args=[
                            name,
                            stmt.name
                        ]
                    )
                )

    def _check_class_variable_annotation(
        self,
        class_def_stmt: ASTClassDefStmt,
        ann_assign_stmt: ASTAnnAssignStmt
    ) -> Flake8Report:
        base_class_name_exprs = ASTUtils.filter(
            nodes=class_def_stmt.bases,
            t=ASTNameExpr
        )

        for base_class_name_expr in base_class_name_exprs:
            if (base_class_name_expr.id == Protocol.__name__):
                return

        annotation_expr = ann_assign_stmt.annotation

        while (True):
            if (isinstance(annotation_expr, ASTNameExpr)):
                annotation_name_expr = annotation_expr

                break

            if (isinstance(annotation_expr, ASTSubscriptExpr)):
                annotation_expr = annotation_expr.value

                continue

            raise SystemException()

        if (annotation_name_expr.id.startswith("Final")):
            return

        if (annotation_name_expr.id.startswith("Mutable")):
            return

        yield Flake8ReportEntryImpl(
            line=ann_assign_stmt.lineno,
            column=ann_assign_stmt.col_offset,
            text="PGN003 Use Final or Mutable annotation"
        )

    def _check_element_indent(
        self,
        owner: ASTNode,
        elements: Array[ASTNode]
    ) -> Flake8Report:
        if (not any(elements)):
            return

        last_element = elements[-1]

        if (last_element.lineno == owner.lineno):
            return

        owner_indent = CollectionUtils.index(
            items=self._lines[owner.lineno - 1],
            fn=lambda char: (char != " ")
        )

        expected_indent = (owner_indent + self.TAB_LENGTH)

        for element in elements:
            actual_indent = CollectionUtils.index(
                items=self._lines[element.lineno - 1],
                fn=lambda char: (char != " ")
            )

            if (expected_indent != actual_indent):
                yield Flake8ReportEntryImpl(
                    line=element.lineno,
                    column=element.col_offset,
                    text="PGN013 Invalid indent"
                )

    def _check_element_starts_on_new_line(
        self,
        owner: ASTNode,
        elements: Iterable[ASTNode]
    ) -> Flake8Report:
        lineno = owner.lineno

        for element in elements:
            if (lineno == element.lineno):
                yield Flake8ReportEntryImpl(
                    line=element.lineno,
                    column=element.col_offset,
                    text="PGN007 Move element to new line"
                )

            lineno = element.lineno

    def _check_empty_line_before(
        self,
        previous_stmt: ASTNode,
        current_stmt: ASTNode
    ) -> Flake8Report:
        types = ArrayImpl.of(
            ASTBreakStmt,
            ASTContinueStmt,
            ASTForStmt,
            ASTIfStmt,
            ASTRaiseStmt,
            ASTReturnStmt,
            ASTTryStmt,
            ASTWhileStmt,
            ASTWithStmt
        )

        if (current_stmt.__class__ not in types):
            return

        if (previous_stmt.end_lineno == (current_stmt.lineno - 1)):
            yield Flake8ReportEntryImpl(
                line=current_stmt.lineno,
                column=current_stmt.col_offset,
                text=TextUtils.format_args(
                    tpl="PGN011 Add empty line before {}",
                    args=[
                        current_stmt.__class__.__name__.lower()
                    ]
                )
            )

    def _check_future_annotations(
        self,
        module: ASTModule
    ) -> Flake8Report:
        if (self._filename.endswith("__init__.py")):
            return

        if (self._filename.endswith(".pyi")):
            return

        for stmt in ASTUtils.filter(module.body, ASTImportFromStmt):
            if (stmt.module != "__future__"):
                continue

            for name in stmt.names:
                if (name.name == "annotations"):
                    return

        yield Flake8ReportEntryImpl(
            line=1,
            column=0,
            text="PGN015 Add from __future__ import annotations"
        )

    def _check_illegal_call(
        self,
        call_expr: ASTCallExpr
    ) -> Flake8Report:
        if (isinstance(call_expr.func, ASTAttributeExpr)):
            func_expr = call_expr.func
        else:
            return

        if (isinstance(func_expr.value, ASTConstantExpr)):
            func_owner_class = func_expr.value.value.__class__
        else:
            return

        if (func_owner_class == str):
            if (func_expr.attr == str.format.__name__):
                yield Flake8ReportEntryImpl(
                    line=func_expr.lineno,
                    column=func_expr.col_offset,
                    text="PGN009 Do not use the \"...\".format"
                )
            elif (func_expr.attr == str.join.__name__):
                yield Flake8ReportEntryImpl(
                    line=func_expr.lineno,
                    column=func_expr.col_offset,
                    text="PGN009 Do not use the \"...\".join"
                )

    def _check_illegal_function_def_returns(
        self,
        returns_expr: ASTNode
    ) -> Flake8Report:
        if (isinstance(returns_expr, ASTSubscriptExpr)):
            subscript_expr = returns_expr
        else:
            return

        if (isinstance(subscript_expr.value, ASTNameExpr)):
            name_expr = subscript_expr.value
        else:
            return

        if (name_expr.id.endswith("Iterable")):
            yield Flake8ReportEntryImpl(
                line=returns_expr.lineno,
                column=returns_expr.col_offset,
                text="PGN006 Replace Iterable with Iterator, or ..."
            )

    def _check_imported_symbol(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        import_from_stmt: ASTImportFromStmt
    ) -> Flake8Report:
        if (import_from_stmt.module is None):
            return
        else:
            module_name = import_from_stmt.module

        for alias in import_from_stmt.names:
            module = cache.modules.get(module_name)

            if (module is None):
                module = self._collect_module_data(module_name)

                cache.modules[module_name] = module

            if (alias.name in module.exported_symbols):
                continue

            yield Flake8ReportEntryImpl(
                line=import_from_stmt.lineno,
                column=import_from_stmt.col_offset,
                text=TextUtils.format_args(
                    tpl="PGN010 Not intended symbol for external use [{}.{}]",
                    args=[
                        module_name,
                        alias.name
                    ]
                )
            )

    def _check_line_length(self) -> Flake8Report:
        for index in range(0, len(self._lines)):
            line = self._lines[index]

            if (line.startswith("from ")):
                continue

            line_length = len(line) - 1

            if (line_length > self.MAX_LINE_LENGTH):
                yield Flake8ReportEntryImpl(
                    line=index + 1,
                    column=self.MAX_LINE_LENGTH + 1,
                    text=TextUtils.format_args(
                        tpl="PGN001 Line too long ({} > {} characters)",
                        args=[
                            line_length,
                            self.MAX_LINE_LENGTH
                        ]
                    )
                )

    def _check_module_variable(
        self,
        ann_assign_stmt: ASTAnnAssignStmt
    ) -> Flake8Report:
        if (isinstance(ann_assign_stmt.target, ASTNameExpr)):
            target_name_expr = ann_assign_stmt.target
        else:
            raise SystemException()

        if (target_name_expr.id != "__all__"):
            yield Flake8ReportEntryImpl(
                line=ann_assign_stmt.lineno,
                column=ann_assign_stmt.col_offset,
                text="PGN012 Not use module variable"
            )

    def _check_pass_using(
        self,
        pass_stmt: ASTPassStmt
    ) -> Flake8Report:
        yield Flake8ReportEntryImpl(
            line=pass_stmt.lineno,
            column=pass_stmt.col_offset,
            text="PGN008 Replace pass with ellipsis"
        )

    def _check_redundant_type_annotation(
        self,
        owner: ASTNode,
        ann_assign_stmt: ASTAnnAssignStmt
    ) -> Flake8Report:
        if (isinstance(owner, ASTModule)):
            return

        if (isinstance(owner, ASTClassDefStmt)):
            return

        yield Flake8ReportEntryImpl(
            line=ann_assign_stmt.lineno,
            column=ann_assign_stmt.annotation.col_offset,
            text="PGN005 Remove type annotation"
        )

    def _check_termination_comma(
        self,
        elements: Array[ASTNode]
    ) -> Flake8Report:
        if (not any(elements)):
            return

        last_element = elements[-1]

        if (last_element.end_lineno is not None):
            line = last_element.end_lineno
        else:
            raise SystemException()

        if (last_element.end_col_offset is not None):
            column = last_element.end_col_offset
        else:
            raise SystemException()

        line_encoded = self._lines[line - 1].encode()

        if (line_encoded[column] == ord(",")):
            yield Flake8ReportEntryImpl(
                line=line,
                column=column,
                text="PGN004 Remove termination comma"
            )

    def _collect_module_data(
        self,
        name: str
    ) -> Flake8CheckerModule:
        module = importlib.import_module(
            name=name
        )

        return Flake8CheckerModule(
            exported_symbols=ArrayImpl.copy(
                instance=module.__dict__.get("__all__", {})
            )
        )

    def _dict_keys(self, dict_expr: ASTDictExpr) -> Iterator[ASTNode]:
        for index in range(0, len(dict_expr.keys)):
            key = dict_expr.keys[index]

            if (key is not None):
                yield key
            else:
                value = dict_expr.values[index]

                yield ASTNode(
                    lineno=value.lineno,
                    col_offset=value.col_offset
                )

    def _find_and_check(
        self,
        nodes: Iterable[ASTNode],
        t: Type[ASTNodeT],
        fn: Callable[[ASTNodeT], Flake8Report]
    ) -> Flake8Report:
        for node in ASTUtils.filter(nodes, t):
            for entry in fn(node):
                yield entry

    def _find_and_process(
        self,
        nodes: Iterable[ASTNode],
        t: Type[ASTNodeT],
        fn: Callable[[ASTNodeT], Flake8Reports]
    ) -> Flake8Reports:
        for node in ASTUtils.filter(nodes, t):
            for reports in fn(node):
                yield iter(reports)

    def _function_args(
        self,
        function_def_stmt: ASTFunctionDefStmt
    ) -> Iterator[ASTNode]:
        args = function_def_stmt.args

        required_args = args.args[:len(args.args) - len(args.defaults)]
        optional_args = ArrayImpl.collect(
            iterator=CollectionUtils.join_by_index(
                first=args.args[len(required_args):],
                second=args.defaults
            )
        )

        for arg in required_args:
            yield arg

        for arg, default in optional_args:
            yield ASTNode(
                lineno=arg.lineno,
                col_offset=arg.col_offset,
                end_lineno=default.end_lineno,
                end_col_offset=default.end_col_offset
            )

        if (args.vararg is not None):
            yield args.vararg

        if (args.kwarg is not None):
            yield args.kwarg

    def _process_ann_assign_stmt(
        self,
        owner: ASTNode,
        ann_assign_stmt: ASTAnnAssignStmt
    ) -> Flake8Reports:
        if (ann_assign_stmt.value is not None):
            yield self._union_reports(
                reports=self._process_expr(ann_assign_stmt.value)
            )

        yield self._check_redundant_type_annotation(owner, ann_assign_stmt)

    def _process_assign_stmt(
        self,
        assign_stmt: ASTAssignStmt
    ) -> Flake8Reports:
        return self._process_expr(assign_stmt.value)

    def _process_aug_assign_stmt(
        self,
        aug_assign_stmt: ASTAugAssignStmt
    ) -> Flake8Reports:
        return EmptyIteratorImpl()

    def _process_body(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        body: Array[ASTNode]
    ) -> Flake8Reports:
        for index in range(1, len(body)):
            yield self._check_empty_line_before(
                previous_stmt=body[index - 1],
                current_stmt=body[index]
            )

        for stmt in body:
            yield self._union_reports(
                reports=self._process_stmt(cache, owner, stmt)
            )

    def _process_call_expr(
        self,
        call_expr: ASTCallExpr
    ) -> Flake8Reports:
        args = ArrayImpl.collect(
            iterator=CollectionUtils.chain(
                links=[
                    call_expr.args,
                    call_expr.keywords
                ]
            )
        )

        yield self._check_illegal_call(call_expr)

        for arg in call_expr.args:
            yield self._union_reports(
                reports=self._process_expr(arg)
            )

        for keyword in call_expr.keywords:
            yield self._union_reports(
                reports=self._process_expr(keyword.value)
            )

        yield self._check_element_indent(call_expr, args)

        yield self._check_termination_comma(args)

        if (any(call_expr.args)):
            yield self._check_call_args(call_expr)

    def _process_class_ann_assign_stmt(
        self,
        class_def_stmt: ASTClassDefStmt,
        ann_assign_stmt: ASTAnnAssignStmt
    ) -> Flake8Reports:
        if (isinstance(ann_assign_stmt.annotation, ASTSubscriptExpr)):
            yield self._union_reports(
                reports=self._process_subscript_expr(
                    subscript_expr=ann_assign_stmt.annotation
                )
            )

        yield self._check_class_variable_annotation(
            class_def_stmt=class_def_stmt,
            ann_assign_stmt=ann_assign_stmt
        )

    def _process_class_def_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        class_def_stmt: ASTClassDefStmt
    ) -> Flake8Reports:
        yield self._union_reports(
            reports=self._find_and_process(
                nodes=class_def_stmt.bases,
                t=ASTSubscriptExpr,
                fn=self._process_subscript_expr
            )
        )

        yield self._union_reports(
            reports=self._find_and_process(
                nodes=class_def_stmt.body,
                t=ASTAnnAssignStmt,
                fn=lambda ann_assign_stmt: self._process_class_ann_assign_stmt(
                    class_def_stmt=class_def_stmt,
                    ann_assign_stmt=ann_assign_stmt
                )
            )
        )

        yield self._check_base_classes(class_def_stmt)

        yield self._check_body(cache, class_def_stmt, class_def_stmt.body)

        yield self._check_class_function_def_order(class_def_stmt)

    def _process_dict_expr(
        self,
        dict_expr: ASTDictExpr
    ) -> Flake8Reports:
        keys = ArrayImpl.collect(
            iterator=self._dict_keys(dict_expr)
        )

        yield self._check_element_indent(dict_expr, keys)

        yield self._check_termination_comma(dict_expr.values)

        yield self._check_element_starts_on_new_line(
            owner=dict_expr,
            elements=keys
        )

    def _process_expr(
        self,
        expr: ASTNode
    ) -> Flake8Reports:
        if (isinstance(expr, ASTAttributeExpr)):
            ...
        elif (isinstance(expr, ASTBinOp)):
            ...
        elif (isinstance(expr, ASTCallExpr)):
            return self._process_call_expr(expr)
        elif (isinstance(expr, ASTCompare)):
            ...
        elif (isinstance(expr, ASTConstantExpr)):
            ...
        elif (isinstance(expr, ASTDictExpr)):
            return self._process_dict_expr(expr)
        elif (isinstance(expr, ASTGeneratorExp)):
            ...
        elif (isinstance(expr, ASTLambdaExpr)):
            return self._process_expr(expr.body)
        elif (isinstance(expr, ASTListExpr)):
            return self._process_list_expr(expr)
        elif (isinstance(expr, ASTNameExpr)):
            ...
        elif (isinstance(expr, ASTStarred)):
            ...
        elif (isinstance(expr, ASTSubscriptExpr)):
            return self._process_subscript_expr(expr)
        elif (isinstance(expr, ASTTupleExpr)):
            ...
        elif (isinstance(expr, ASTUnaryOp)):
            ...
        elif (isinstance(expr, ASTYieldExpr)):
            value = expr.value

            if (value is not None):
                return self._process_expr(value)
        else:
            raise Exception(expr.__class__.__name__)

        return EmptyIteratorImpl()

    def _process_expr_stmt(
        self,
        expr_stmt: ASTExprStmt
    ) -> Flake8Reports:
        return self._process_expr(expr_stmt.value)

    def _process_for_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        for_stmt: ASTForStmt
    ) -> Flake8Reports:
        yield self._check_body(cache, for_stmt, for_stmt.body)

        yield self._check_body(cache, for_stmt, for_stmt.orelse)

        if (isinstance(for_stmt.iter, ASTCallExpr)):
            call_expr = for_stmt.iter

            yield self._union_reports(
                reports=self._process_call_expr(call_expr)
            )

    def _process_function_def_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        function_def_stmt: ASTFunctionDefStmt
    ) -> Flake8Reports:
        args = ArrayImpl.collect(
            iterator=self._function_args(function_def_stmt)
        )

        returns = function_def_stmt.returns

        yield self._check_element_indent(function_def_stmt, args)

        yield self._check_termination_comma(args)

        if (returns is not None):
            yield self._check_illegal_function_def_returns(returns)

            if (function_def_stmt.lineno != returns.lineno):
                yield self._check_element_starts_on_new_line(
                    owner=function_def_stmt,
                    elements=args
                )

        yield self._check_body(
            cache=cache,
            owner=function_def_stmt,
            body=function_def_stmt.body
        )

    def _process_if_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        if_stmt: ASTIfStmt
    ) -> Flake8Reports:
        yield self._check_body(cache, if_stmt, if_stmt.body)

        yield self._check_body(cache, if_stmt, if_stmt.orelse)

    def _process_import_from_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        import_from_stmt: ASTImportFromStmt
    ) -> Flake8Reports:
        yield self._check_imported_symbol(cache, owner, import_from_stmt)

    def _process_import_stmt(
        self,
        import_stmt: ASTImportStmt
    ) -> Flake8Reports:
        return EmptyIteratorImpl()

    def _process_list_expr(
        self,
        list_expr: ASTListExpr
    ) -> Flake8Reports:
        if (not any(list_expr.elts)):
            return

        for elt_expr in list_expr.elts:
            yield self._union_reports(
                reports=self._process_expr(elt_expr)
            )

        yield self._check_element_indent(list_expr, list_expr.elts)

        yield self._check_termination_comma(list_expr.elts)

        if (list_expr.lineno != list_expr.end_lineno):
            yield self._check_element_starts_on_new_line(
                owner=list_expr,
                elements=list_expr.elts
            )

    def _process_pass_stmt(
        self,
        pass_stmt: ASTPassStmt
    ) -> Flake8Reports:
        yield self._check_pass_using(pass_stmt)

    def _process_raise_stmt(
        self,
        raise_stmt: ASTRaiseStmt
    ) -> Flake8Reports:
        if (isinstance(raise_stmt.exc, ASTCallExpr)):
            call_expr = raise_stmt.exc

            yield self._union_reports(
                reports=self._process_call_expr(call_expr)
            )

    def _process_return_stmt(
        self,
        return_stmt: ASTReturnStmt
    ) -> Flake8Reports:
        value_expr = return_stmt.value

        if (value_expr is not None):
            yield self._union_reports(
                reports=self._process_expr(value_expr)
            )

    def _process_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        stmt: ASTNode
    ) -> Flake8Reports:
        if (isinstance(stmt, ASTPassStmt)):
            return self._process_pass_stmt(stmt)

        if (isinstance(stmt, ASTImportStmt)):
            return self._process_import_stmt(stmt)

        if (isinstance(stmt, ASTImportFromStmt)):
            return self._process_import_from_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTClassDefStmt)):
            return self._process_class_def_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTFunctionDefStmt)):
            return self._process_function_def_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTIfStmt)):
            return self._process_if_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTForStmt)):
            return self._process_for_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTWhileStmt)):
            return self._process_while_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTTryStmt)):
            return self._process_try_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTWithStmt)):
            return self._process_with_stmt(cache, owner, stmt)

        if (isinstance(stmt, ASTExprStmt)):
            return self._process_expr_stmt(stmt)

        if (isinstance(stmt, ASTRaiseStmt)):
            return self._process_raise_stmt(stmt)

        if (isinstance(stmt, ASTAnnAssignStmt)):
            return self._process_ann_assign_stmt(owner, stmt)

        if (isinstance(stmt, ASTAugAssignStmt)):
            return self._process_aug_assign_stmt(stmt)

        if (isinstance(stmt, ASTAssignStmt)):
            return self._process_assign_stmt(stmt)

        if (isinstance(stmt, ASTReturnStmt)):
            return self._process_return_stmt(stmt)

        if (isinstance(stmt, ASTBreakStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTContinueStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTAssertStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTDeleteStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTGlobalStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTAsyncFunctionDefStmt)):
            return EmptyIteratorImpl()

        if (isinstance(stmt, ASTNonlocalStmt)):
            return EmptyIteratorImpl()

        raise SystemException(stmt.__class__.__name__)

    def _process_subscript_expr(
        self,
        subscript_expr: ASTSubscriptExpr
    ) -> Flake8Reports:
        if (isinstance(subscript_expr.slice, ASTTupleExpr)):
            elts = subscript_expr.slice.elts
        else:
            elts = [
                subscript_expr.slice
            ]

        yield self._check_element_indent(subscript_expr, elts)

        yield self._check_termination_comma(elts)

        if (subscript_expr.lineno != subscript_expr.end_lineno):
            yield self._check_element_starts_on_new_line(
                owner=subscript_expr,
                elements=elts
            )

        yield self._union_reports(
            reports=self._find_and_process(
                nodes=elts,
                t=ASTSubscriptExpr,
                fn=self._process_subscript_expr
            )
        )

    def _process_try_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        try_stmt: ASTTryStmt
    ) -> Flake8Reports:
        yield self._check_body(cache, try_stmt, try_stmt.body)

        yield self._union_reports(
            reports=self._process_body(cache, owner, try_stmt.finalbody)
        )

        for handler in try_stmt.handlers:
            yield self._union_reports(
                reports=self._process_body(
                    cache=cache,
                    owner=handler,
                    body=handler.body
                )
            )

    def _process_while_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        while_stmt: ASTWhileStmt
    ) -> Flake8Reports:
        return self._process_body(
            cache=cache,
            owner=while_stmt,
            body=while_stmt.body
        )

    def _process_with_stmt(
        self,
        cache: Flake8CheckerCache,
        owner: ASTNode,
        with_stmt: ASTWithStmt
    ) -> Flake8Reports:
        yield self._union_reports(
            reports=self._find_and_process(
                nodes=CollectionUtils.map(
                    items=with_stmt.items,
                    fn=lambda item: item.context_expr
                ),
                t=ASTCallExpr,
                fn=self._process_call_expr
            )
        )

        yield self._check_body(cache, with_stmt, with_stmt.body)

    def _union_reports(
        self,
        reports: Flake8Reports
    ) -> Flake8Report:
        for report in reports:
            for entry in report:
                yield entry


__all__: FinalCollection[str] = [
    "Flake8CheckerImpl"
]
