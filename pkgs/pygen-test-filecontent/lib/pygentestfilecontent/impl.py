from __future__ import annotations

from typing import Final

from pygenpath.abc import Filepath
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class FileContentTestCaseImpl(TestCase):
    _filepath: Final[Filepath]
    _expected: Final[str]

    def __init__(self, filepath: Filepath, expected: str) -> None:
        self._filepath = filepath
        self._expected = expected

    def run(self, test: Test) -> None:
        with (open(self._filepath.path()) as f):
            actual = f.read()

        test.assert_text_equal(
            expected=self._expected,
            actual=actual,
            msg=lambda args: TextUtils.format_args(
                tpl="{}\n{}",
                args=[
                    self._filepath,
                    args.diff
                ]
            )
        )


__all__: FinalCollection[str] = [
    "FileContentTestCaseImpl"
]
