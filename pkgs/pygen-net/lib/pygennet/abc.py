from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import Pair


@dataclass
class NetworkAddress(object):
    host: Final[str]
    port: Final[int]

    def __str__(self) -> str:
        raise SystemException()

    def args(self) -> Pair[str, int]:
        raise SystemException()


__all__: FinalCollection[str] = [
    "NetworkAddress"
]
