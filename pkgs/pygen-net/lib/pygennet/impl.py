from __future__ import annotations

from pygennet.abc import NetworkAddress
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Pair


class NetworkAddressImpl(NetworkAddress):
    def __str__(self) -> str:
        return TextUtils.format_args(
            tpl="{}:{}",
            args=[
                self.host,
                self.port
            ]
        )

    def args(self) -> Pair[str, int]:
        return (self.host, self.port)


__all__: FinalCollection[str] = [
    "NetworkAddressImpl"
]
