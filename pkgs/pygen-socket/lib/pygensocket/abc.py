from __future__ import annotations

from contextlib import AbstractContextManager

from pygenerr.err import SystemException
from pygennet.abc import NetworkAddress
from pygentype.abc import FinalCollection


class Socket(object):
    def close(self) -> None:
        raise SystemException()

    def fileno(self) -> int:
        raise SystemException()

    def receive(self, buffer_size: int) -> bytes:
        raise SystemException()

    def send(self, data: bytes) -> None:
        raise SystemException()


class SocketContextManager(AbstractContextManager[Socket], Socket):
    def __enter__(self) -> Socket:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()


class ClientSocket(Socket):
    def connect(self, address: NetworkAddress) -> None:
        raise SystemException()


class ServerSocket(object):
    def accept(self) -> SocketContextManager:
        raise SystemException()

    def address(self) -> NetworkAddress:
        raise SystemException()

    def bind(self, address: NetworkAddress) -> None:
        raise SystemException()

    def close(self) -> None:
        raise SystemException()

    def listen(self, backlog: int) -> None:
        raise SystemException()


class SocketPair(object):
    def read(self) -> Socket:
        raise SystemException()

    def write(self) -> Socket:
        raise SystemException()


class SocketPairContextManager(AbstractContextManager[SocketPair], SocketPair):
    def __enter__(self) -> SocketPair:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "ClientSocket",
    "ServerSocket",
    "Socket",
    "SocketContextManager",
    "SocketPair",
    "SocketPairContextManager"
]
