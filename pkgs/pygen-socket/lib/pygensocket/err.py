from __future__ import annotations

from pygenerr.err import UserException
from pygentype.abc import FinalCollection


class SocketClosedException(UserException):
    def __init__(self) -> None:
        super().__init__("Socket closed")


__all__: FinalCollection[str] = [
    "SocketClosedException"
]
