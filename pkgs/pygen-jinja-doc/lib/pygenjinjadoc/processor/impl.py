from __future__ import annotations

from collections.abc import Iterable
from io import StringIO

from pygenalphabet.utils import AlphabetUtils
from pygencollection.impl import ArrayImpl
from pygencollection.impl import StrListImpl
from pygencounter.abc import Counter
from pygencounter.impl import CounterImpl
from pygenjinjadoc.abc import JinjaDocProcessor
from pygenjinjadoc.abc import JinjaDocTokenBeginFor
from pygenjinjadoc.abc import JinjaDocTokenEndFor
from pygenjinjadoc.abc import JinjaDocTokenEOF
from pygenjinjadoc.abc import JinjaDocTokenNonprintingText
from pygenjinjadoc.abc import JinjaDocTokenText
from pygentextdoc.abc import TextDocProcessorResult
from pygentextdoc.processor.impl import TextDocProcessorCacheImpl
from pygentextdoc.processor.impl import TextDocProcessorForkImpl
from pygentextdoc.processor.impl import TextDocProcessorResultFailureImpl
from pygentextdoc.processor.impl import TextDocProcessorResultSuccessImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygenyamldoc.processor.impl import YamlDocProcessorResultFailureImpl


class JinjaDocProcessorResultSuccessImpl(TextDocProcessorResultSuccessImpl):
    ...


class JinjaDocProcessorResultFailureImpl(TextDocProcessorResultFailureImpl):
    ...


class JinjaDocProcessorForkImpl(
    TextDocProcessorForkImpl[JinjaDocProcessor],
    JinjaDocProcessor
):
    def __init__(
        self,
        processors: Iterable[JinjaDocProcessor]
    ) -> None:
        super().__init__(
            processors=processors,
            on_failure=JinjaDocProcessorResultFailureImpl()
        )


class JinjaDocProcessorCacheImpl(TextDocProcessorCacheImpl, JinjaDocProcessor):
    ...


class JinjaDocProcessorFailureImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        return JinjaDocProcessorResultFailureImpl()


class JinjaDocProcessorImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        processor = JinjaDocProcessorForkImpl(
            processors=ArrayImpl.of(
                JinjaDocProcessorTextImpl(),
                JinjaDocProcessorBeginForInImpl(),
                JinjaDocProcessorEOFImpl()
            )
        )

        return processor.process(reader)


class JinjaDocProcessorTextImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        part = StringIO()
        parts = StrListImpl()
        line = reader.line()
        index = CounterImpl()

        while (True):
            char = reader.peek(index.inc())

            if (char == ""):
                return YamlDocProcessorResultFailureImpl()
            elif (char == "%"):
                next_char = reader.peek(index.inc())

                if (next_char == "}"):
                    return YamlDocProcessorResultFailureImpl()
                else:
                    part.write(char)
                    part.write(next_char)
            elif (char == "{"):
                next_char = reader.peek(index.inc())

                if (next_char == "%"):
                    return YamlDocProcessorResultFailureImpl()
                elif (next_char == "{"):
                    expression = self._process_expression(
                        reader=reader,
                        index=index
                    )

                    if (expression is None):
                        return YamlDocProcessorResultFailureImpl()
                    else:
                        parts.add(part.getvalue())
                        part.truncate(0)
                        part.seek(0)
                else:
                    part.write(char)
                    part.write(next_char)
            elif (char == "\n"):
                reader.forward(index.value() - 1)

                processor = JinjaDocProcessorNonprintingTextImpl()

                result = processor.process(reader)

                if (result.success()):
                    next = JinjaDocProcessorCacheImpl(
                        result=result
                    )
                else:
                    next = JinjaDocProcessorImpl()

                    part.write(char)

                    reader.forward(1)

                parts.add(part.getvalue())

                return JinjaDocProcessorResultSuccessImpl(
                    next=next,
                    token=JinjaDocTokenText(
                        parts=ArrayImpl.copy(parts)
                    ),
                    line=line
                )
            else:
                part.write(char)

    def _process_expression(
        self,
        reader: TextReader,
        index: Counter
    ) -> Optional[str]:
        value = StringIO()

        if (reader.peek(index.inc()) != " "):
            return None

        char = reader.peek(index.inc())

        if (char in AlphabetUtils.LATIN):
            value.write(char)
        else:
            return None

        while (True):
            char = reader.peek(index.inc())

            if (char in AlphabetUtils.LATIN):
                value.write(char)
            elif (char in AlphabetUtils.NUMBERS):
                value.write(char)
            elif (char == "."):
                next_char = reader.peek(index.inc())

                if (next_char not in AlphabetUtils.LATIN):
                    return None

                value.write(char)
                value.write(next_char)
            elif (char == " "):
                break
            else:
                return None

        for char in "}}":
            if (char != reader.peek(index.inc())):
                return None

        return value.getvalue()


class JinjaDocProcessorNonprintingTextImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        char_count = CounterImpl()
        line = reader.line()
        index = CounterImpl()

        if (reader.peek(index.inc()) == "\n"):
            char_count.inc()
        else:
            return YamlDocProcessorResultFailureImpl()

        while (True):
            char = reader.peek(index.inc())

            if (char in (" ", "\n")):
                char_count.inc()
            elif (char == "{"):
                break
            else:
                return YamlDocProcessorResultFailureImpl()

        for char in "%-":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(char_count.value())

        return JinjaDocProcessorResultSuccessImpl(
            next=JinjaDocProcessorImpl(),
            token=JinjaDocTokenNonprintingText(),
            line=line
        )


class JinjaDocProcessorBeginForInImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "{%- for ":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        if (not self._process_variable(reader, index)):
            return YamlDocProcessorResultFailureImpl()

        for char in "in ":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        if (not self._process_array(reader, index)):
            return YamlDocProcessorResultFailureImpl()

        for char in "%}\n":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return JinjaDocProcessorResultSuccessImpl(
            next=JinjaDocProcessorEndForImpl(),
            token=JinjaDocTokenBeginFor(),
            line=line
        )

    def _process_array(
        self,
        reader: TextReader,
        index: Counter
    ) -> bool:
        if (reader.peek(index.inc()) not in AlphabetUtils.LATIN):
            return False

        while (True):
            char = reader.peek(index.inc())

            if (char in AlphabetUtils.LATIN):
                continue
            elif (char == "."):
                next_char = reader.peek(index.inc())

                if (next_char in AlphabetUtils.LATIN):
                    continue
                else:
                    return False
            elif (char == " "):
                return True
            else:
                return False

    def _process_variable(
        self,
        reader: TextReader,
        index: Counter
    ) -> bool:
        if (reader.peek(index.inc()) not in AlphabetUtils.LATIN):
            return False

        while (True):
            char = reader.peek(index.inc())

            if (char in AlphabetUtils.LATIN):
                continue
            elif (char in ("_")):
                continue
            elif (char == " "):
                return True
            else:
                return False


class JinjaDocProcessorEndForImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        body_result = self._process_body(reader)

        if (body_result is not None):
            return body_result

        statement_result = self._process_statement(reader)

        return statement_result

    def _create_statement_tail_processor(
        self,
        reader: TextReader
    ) -> JinjaDocProcessor:
        processor = JinjaDocProcessorNonprintingTextImpl()

        result = processor.process(reader)

        if (result.success()):
            return JinjaDocProcessorCacheImpl(
                result=result
            )
        else:
            return JinjaDocProcessorImpl()

    def _process_body(
        self,
        reader: TextReader
    ) -> Optional[TextDocProcessorResult]:
        processor = JinjaDocProcessorTextImpl()

        result = processor.process(reader)

        if (result.success()):
            return JinjaDocProcessorResultSuccessImpl(
                next=self,
                token=result.token(),
                line=result.line()
            )
        else:
            return None

    def _process_statement(
        self,
        reader: TextReader
    ) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "{%- endfor %}\n":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value() - 1)

        return JinjaDocProcessorResultSuccessImpl(
            next=self._create_statement_tail_processor(reader),
            token=JinjaDocTokenEndFor(),
            line=line
        )


class JinjaDocProcessorEOFImpl(JinjaDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        char = reader.peek(0)

        if (char == ""):
            return JinjaDocProcessorResultSuccessImpl(
                next=JinjaDocProcessor(),
                token=JinjaDocTokenEOF(),
                line=reader.line()
            )
        else:
            return JinjaDocProcessorResultFailureImpl()


__all__: FinalCollection[str] = [
    "JinjaDocProcessorImpl"
]
