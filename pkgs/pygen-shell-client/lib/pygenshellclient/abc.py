from __future__ import annotations

from collections.abc import Collection
from contextlib import AbstractContextManager

from pygenerr.err import SystemException
from pygennet.abc import NetworkAddress
from pygenshell.abc import ShellOutput
from pygentype.abc import FinalCollection


class ShellClientSession(object):
    def call(
        self,
        args: Collection[str],
        output: ShellOutput
    ) -> None:
        raise SystemException()


class ShellClientSessionContextManager(
    AbstractContextManager[ShellClientSession]
):
    def __enter__(self) -> ShellClientSession:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()


class ShellClient(object):
    def connect(
        self,
        address: NetworkAddress
    ) -> ShellClientSessionContextManager:
        raise SystemException()


__all__: FinalCollection[str] = [
    "ShellClient",
    "ShellClientSession",
    "ShellClientSessionContextManager"
]
