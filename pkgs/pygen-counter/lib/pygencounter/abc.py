from __future__ import annotations

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class Counter(object):
    def dec(self) -> int:
        raise SystemException()

    def inc(self) -> int:
        raise SystemException()

    def update(self, value: int) -> None:
        raise SystemException()

    def value(self) -> int:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Counter"
]
