from __future__ import annotations

from collections.abc import Iterable
from typing import Final
from typing import Generic

from pygencollection.impl import ArrayImpl
from pygentextdoc.abc import TextDocProcessor
from pygentextdoc.abc import TextDocProcessorResult
from pygentextdoc.abc import TextDocProcessorT
from pygentextdoc.abc import TextDocToken
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection


class TextDocProcessorResultSuccessImpl(TextDocProcessorResult):
    _next: Final[TextDocProcessor]
    _token: Final[TextDocToken]
    _line: Final[int]

    def __init__(
        self,
        next: TextDocProcessor,
        token: TextDocToken,
        line: int
    ) -> None:
        self._next = next
        self._token = token
        self._line = line

    def line(self) -> int:
        return self._line

    def next(self) -> TextDocProcessor:
        return self._next

    def success(self) -> bool:
        return True

    def token(self) -> TextDocToken:
        return self._token


class TextDocProcessorResultFailureImpl(TextDocProcessorResult):
    def success(self) -> bool:
        return False


class TextDocProcessorForkImpl(Generic[TextDocProcessorT], TextDocProcessor):
    _processors: FinalCollection[TextDocProcessorT]
    _on_failure: Final[TextDocProcessorResult]

    def __init__(
        self,
        processors: Iterable[TextDocProcessorT],
        on_failure: TextDocProcessorResult
    ) -> None:
        self._processors = ArrayImpl.copy(processors)
        self._on_failure = on_failure

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        for processor in self._processors:
            result = processor.process(reader)

            if (result.success()):
                return result

        return self._on_failure


class TextDocProcessorCacheImpl(TextDocProcessor):
    _result: Final[TextDocProcessorResult]

    def __init__(self, result: TextDocProcessorResult) -> None:
        self._result = result

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        return self._result


class TextDocProcessorLnImpl(TextDocProcessor):
    _on_success: Final[TextDocProcessorResult]
    _on_failure: Final[TextDocProcessorResult]

    def __init__(
        self,
        on_success: TextDocProcessorResult,
        on_failure: TextDocProcessorResult
    ) -> None:
        self._on_success = on_success
        self._on_failure = on_failure

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        if (reader.peek(0) == "\n"):
            reader.forward(1)

            return self._on_success
        else:
            return self._on_failure


__all__: FinalCollection[str] = [
    "TextDocProcessorCacheImpl",
    "TextDocProcessorForkImpl",
    "TextDocProcessorLnImpl",
    "TextDocProcessorResultFailureImpl",
    "TextDocProcessorResultSuccessImpl"
]
