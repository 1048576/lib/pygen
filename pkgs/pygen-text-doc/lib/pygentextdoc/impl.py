from __future__ import annotations

from typing import Final

from pygenerr.err import UserException
from pygentext.utils import TextUtils
from pygentextdoc.abc import TextDocLoader
from pygentextdoc.abc import TextDocProcessor
from pygentextdoc.abc import TextDocStream
from pygentextdoc.abc import TextDocStreamNode
from pygentextdoc.abc import TextDocToken
from pygentextreader.abc import TextReader
from pygentextreader.impl import TextFileReaderImpl
from pygentype.abc import FinalCollection
from pygentype.abc import FinalType
from pygentype.abc import Type


class TextDocLoaderImpl(TextDocLoader):
    _processor: Final[TextDocProcessor]
    _eof_token_type: FinalType[TextDocToken]

    def __init__(
        self,
        processor: TextDocProcessor,
        eof_token_type: Type[TextDocToken]
    ) -> None:
        self._processor = processor
        self._eof_token_type = eof_token_type

    def load_stream_from_file(self, path: str) -> TextDocStream:
        with (TextFileReaderImpl(path) as reader):
            for node in self.load_stream_from_reader(reader):
                yield node

    def load_stream_from_reader(self, reader: TextReader) -> TextDocStream:
        processor = self._processor

        while (True):
            result = processor.process(reader)

            if (result.success()):
                node = TextDocStreamNode(
                    token=result.token(),
                    line=result.line()
                )

                yield node
            else:
                raise UserException(
                    msg=TextUtils.format_args(
                        tpl="Invalid document [{}]",
                        args=[
                            reader
                        ]
                    )
                )

            if (result.token().__class__ == self._eof_token_type):
                break

            processor = result.next()


__all__: FinalCollection[str] = [
    "TextDocLoaderImpl"
]
