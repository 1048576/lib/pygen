from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass
from typing import Final
from typing import TypeVar

from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection


@dataclass
class TextDocToken(DataclassInstance):
    ...


class TextDocProcessorResult(object):
    def line(self) -> int:
        raise SystemException()

    def next(self) -> TextDocProcessor:
        raise SystemException()

    def success(self) -> bool:
        raise SystemException()

    def token(self) -> TextDocToken:
        raise SystemException()


class TextDocProcessor(object):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        raise SystemException()


@dataclass
class TextDocStreamNode(object):
    token: Final[TextDocToken]
    line: Final[int]


TextDocStream = Iterable[TextDocStreamNode]


class TextDocLoader(object):
    def load_stream_from_file(self, path: str) -> TextDocStream:
        raise SystemException()

    def load_stream_from_reader(self, reader: TextReader) -> TextDocStream:
        raise SystemException()


TextDocProcessorT = TypeVar(
    name="TextDocProcessorT",
    bound=TextDocProcessor
)

__all__: FinalCollection[str] = [
    "TextDocLoader",
    "TextDocProcessor",
    "TextDocProcessorResult",
    "TextDocProcessorT",
    "TextDocStream",
    "TextDocStreamNode",
    "TextDocToken"
]
