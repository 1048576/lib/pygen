from __future__ import annotations

from collections.abc import Mapping

from pygenarg.abc import Arg
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class TextArgContext(object):
    def value(self) -> str:
        raise SystemException()


class TextArg(Arg[TextArgContext], TextArgContext):
    ...


class TextArgMapper(object):
    def optional(self, description: str, value: str) -> TextArg:
        raise SystemException()

    def optional_env(
        self,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        raise SystemException()

    def required(self, description: str, value: str) -> TextArg:
        raise SystemException()

    def required_env(
        self,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        raise SystemException()


__all__: FinalCollection[str] = [
    "TextArg",
    "TextArgContext",
    "TextArgMapper"
]
