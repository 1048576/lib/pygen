from __future__ import annotations

from pygenerr.err import UserException
from pygenpath.abc import Filepath
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class ASTSyntaxException(UserException):
    @classmethod
    def argument_no_found(
        cls,
        what: str,
        filepath: Filepath,
        lineno: int,
        name: str
    ) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl=(
                    "{} contains error:\n"
                    "  line: {}:{}\n"
                    "  error: argument [{}] not found:\n"
                ),
                args=[
                    what,
                    filepath.path(),
                    lineno,
                    name
                ]
            )
        )

    @classmethod
    def invalid_type(
        cls,
        what: str,
        filepath: Filepath,
        lineno: int,
        varname: str,
        expected_type: str,
        actual_type: str
    ) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl=(
                    "{} contains error:\n"
                    "  line: {}:{}\n"
                    "  error: {} is [{}] instead of {}"
                ),
                args=[
                    what,
                    filepath.path(),
                    lineno,
                    varname,
                    actual_type,
                    expected_type
                ]
            )
        )


__all__: FinalCollection[str] = [
    "ASTSyntaxException"
]
