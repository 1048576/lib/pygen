from __future__ import annotations

from contextlib import AbstractContextManager
from typing import Final

from pygentest.abc import Test
from pygentest.abc import TestAssertExceptionEqualMsg
from pygentest.abc import TestAssertExceptionEqualMsgArgs
from pygentest.abc import TestAssertObjectEqualMsg
from pygentest.abc import TestAssertObjectEqualMsgArgs
from pygentest.abc import TestAssertScalarEqualMsg
from pygentest.abc import TestAssertScalarEqualMsgArgs
from pygentest.abc import TestAssertTextEqualMsg
from pygentest.abc import TestAssertTextEqualMsgArgs
from pygentest.abc import TestCase
from pygentest.abc import TestExceptionCatcher
from pygentest.abc import TestSuite
from pygentest.impl import TestAssertExceptionEqualMsgImpl
from pygentest.impl import TestAssertObjectEqualMsgImpl
from pygentest.impl import TestAssertScalarEqualMsgImpl
from pygentest.impl import TestAssertTextEqualMsgImpl
from pygentestunittest.abc import UnitTestCase
from pygentestunittest.abc import UnitTestSuite
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import ScalarT
from pygentype.abc import Type


class UnitTestImpl(Test):
    _unit_test_case: Final[UnitTestCase]

    def __init__(self, unit_test_case: UnitTestCase) -> None:
        self._unit_test_case = unit_test_case

    def assert_exception_equal(
        self,
        expected: Exception,
        exception_catcher: TestExceptionCatcher,
        msg: TestAssertExceptionEqualMsg = TestAssertExceptionEqualMsgImpl()
    ) -> None:
        actual = exception_catcher.exception

        try:
            self._unit_test_case.assertEqual(
                first=expected,
                second=actual,
                msg=""
            )

            return
        except AssertionError:
            args = TestAssertExceptionEqualMsgArgs(
                expected=expected,
                actual=actual
            )

        raise AssertionError(msg(args))

    def assert_object_equal(
        self,
        expected: object,
        actual: object,
        msg: TestAssertObjectEqualMsg = TestAssertObjectEqualMsgImpl()
    ) -> None:
        try:
            self._unit_test_case.assertEqual(
                first=expected,
                second=actual
            )

            return
        except AssertionError:
            args = TestAssertObjectEqualMsgArgs(
                expected=expected,
                actual=actual
            )

        raise AssertionError(msg(args))

    def assert_scalar_equal(
        self,
        expected: ScalarT,
        actual: ScalarT,
        msg: TestAssertScalarEqualMsg = TestAssertScalarEqualMsgImpl()
    ) -> None:
        try:
            self._unit_test_case.assertEqual(
                first=expected,
                second=actual,
                msg=""
            )

            return
        except AssertionError:
            args = TestAssertScalarEqualMsgArgs(
                expected=expected,
                actual=actual
            )

        raise AssertionError(msg(args))

    def assert_text_equal(
        self,
        expected: str,
        actual: str,
        msg: TestAssertTextEqualMsg = TestAssertTextEqualMsgImpl()
    ) -> None:
        try:
            self._unit_test_case.assertMultiLineEqual(
                first=expected,
                second=actual
            )

            return
        except AssertionError as e:
            args = TestAssertTextEqualMsgArgs(
                expected=expected,
                actual=actual,
                diff=e.args[0][e.args[0].find("\n") + 1:]
            )

        raise AssertionError(msg(args))

    def exception_catcher(
        self,
        exception_type: Type[Exception] = Exception
    ) -> TestExceptionCatcher:
        return self._unit_test_case.assertRaises(
            expected_exception=exception_type,
            msg=TextUtils.format_args(
                tpl="[{}] not raized",
                args=[
                    exception_type.__name__
                ]
            )
        )

    def sub_test(self, msg: str) -> AbstractContextManager[None]:
        return self._unit_test_case.subTest(msg)


class UnitTestCaseImpl(UnitTestCase):
    _test_case: Final[TestCase]

    def __init__(
        self,
        test_case: TestCase
    ) -> None:
        super().__init__()

        self._test_case = test_case
        self.maxDiff = 10000

    def __str__(self) -> str:
        return TextUtils.format_args(
            tpl="{}.{}",
            args=[
                self._test_case.__class__.__module__,
                self._test_case.__class__.__qualname__
            ]
        )

    def runTest(self) -> None:
        self._test_case.run(
            test=UnitTestImpl(self)
        )


class UnitTestSuiteImpl(UnitTestSuite, TestSuite):
    def add_test_case(self, test_case: TestCase) -> None:
        self.addTest(
            test=UnitTestCaseImpl(test_case)
        )


__all__: FinalCollection[str] = [
    "UnitTestImpl",
    "UnitTestSuiteImpl"
]
