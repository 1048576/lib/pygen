from __future__ import annotations

from unittest import TestCase
from unittest import TestSuite

from pygentype.abc import FinalCollection


class UnitTestCase(TestCase):
    ...


class UnitTestSuite(TestSuite):
    ...


__all__: FinalCollection[str] = [
    "UnitTestCase",
    "UnitTestSuite"
]
