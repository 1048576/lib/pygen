from __future__ import annotations

from types import ModuleType

from pygenpath.abc import Dirpath
from pygentest.abc import TestSuite
from pygentestsample.abc import SampleTestCreateTestCaseFn
from pygentestsample.impl import SampleTestCaseLoaderImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class SampleTestUtils(object):
    @classmethod
    def load(
        cls,
        workspace_dirpath: Dirpath,
        module: ModuleType,
        create_test_case_fn: SampleTestCreateTestCaseFn,
        test_suite: TestSuite
    ) -> None:
        loader = SampleTestCaseLoaderImpl(
            dirpath=workspace_dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./samples/pkgs/{}/",
                    args=[
                        TextUtils.replace(
                            text=module.__name__,
                            old=".",
                            new="/"
                        )
                    ]
                )
            ),
            create_test_case_fn=create_test_case_fn
        )

        loader.load(test_suite)


__all__: FinalCollection[str] = [
    "SampleTestUtils"
]
