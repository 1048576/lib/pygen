from __future__ import annotations

from typing import IO
from typing import Final

from pygenerr.err import SystemException
from pygentext.utils import TextUtils
from pygentextreader.abc import TextFileReader
from pygentextreader.abc import TextReader
from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygentype.abc import Mutable
from pygentype.abc import MutableOptional


class TextReaderImpl(TextReader):
    _name: Final[str]
    _io: Final[IO[str]]
    _data_window: Mutable[str]
    _pointer: Mutable[int]
    _line: Mutable[int]
    _column: Mutable[int]

    def __init__(
        self,
        name: str,
        io: IO[str]
    ) -> None:
        self._name = name
        self._io = io
        self._data_window = ""
        self._pointer = 0
        self._line = 1
        self._column = 1

    def __str__(self) -> str:
        return TextUtils.format_kwargs(
            tpl=self._name,
            line=self._line
        )

    def eof(self) -> bool:
        return (self.peek(1) == "")

    def forward(self, length: int) -> None:
        if (not self._ensure_enough_data(length - 1)):
            raise SystemException()

        for pointer in range(self._pointer, self._pointer + length):
            if (self._data_window[pointer] == "\n"):
                self._line += 1
                self._column = 1
            else:
                self._column += 1

        self._pointer += length

    def line(self) -> int:
        return self._line

    def peek(self, index: int) -> str:
        if (self._ensure_enough_data(index)):
            return self._data_window[self._pointer + index]
        else:
            return ""

    def peek_position(self, index: int) -> TextReaderPosition:
        if (self._ensure_enough_data(index)):
            text = self._data_window[self._pointer:index]
        else:
            text = self._data_window[self._pointer:]

        line = self._line
        column = self._column

        for char in text:
            if (char == "\n"):
                line += 1
                column = 1
            else:
                column += 1

        return TextReaderPosition(line, column)

    def position(self) -> TextReaderPosition:
        return TextReaderPosition(
            line=self._line,
            column=self._column
        )

    def _ensure_enough_data(self, index: int) -> bool:
        update_size = (self._pointer + index - len(self._data_window) + 1)

        if (update_size > 0):
            self._update(update_size)

        return ((self._pointer + index) < len(self._data_window))

    def _update(self, size: int) -> None:
        suffix = self._io.read(size)

        if (suffix == ""):
            return

        if (self._pointer > 0):
            self._data_window = (self._data_window[self._pointer:] + suffix)
        else:
            self._data_window += suffix

        self._pointer = 0


class TextFileReaderImpl(TextFileReader):
    _path: Final[str]
    _io: MutableOptional[IO[str]]

    def __init__(
        self,
        path: str
    ) -> None:
        self._path = path
        self._io = None

    def __enter__(self) -> TextReader:
        if (self._io is None):
            io = open(self._path)

            self._io = io

            return TextReaderImpl(
                name=TextUtils.format_args(
                    tpl="{}:{{line}}",
                    args=[
                        self._path
                    ]
                ),
                io=io
            )
        else:
            raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (self._io is not None):
            self._io.close()
            self._io = None
        else:
            raise SystemException()


__all__: FinalCollection[str] = [
    "TextFileReaderImpl",
    "TextReaderImpl"
]
