from __future__ import annotations

import re

from pygentype.abc import FinalCollection

RegexPattern = re.Pattern[str]


__all__: FinalCollection[str] = [
    "RegexPattern"
]
