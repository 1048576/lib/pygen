from __future__ import annotations

import re

from pygenregex.abc import RegexPattern
from pygentype.abc import FinalCollection


class RegexUtils(object):
    @classmethod
    def compile(cls, pattern: str) -> RegexPattern:
        return re.compile(pattern)


__all__: FinalCollection[str] = [
    "RegexUtils"
]
