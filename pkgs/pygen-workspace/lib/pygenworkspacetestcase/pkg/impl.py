from __future__ import annotations

from typing import Final

from pygencollection.utils import CollectionUtils
from pygenerr.err import UserException
from pygenjinja.abc import JinjaTemplate
from pygenpath.utils import PathUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspacePkg
from pygenworkspacetestcase.abc import WorkspacePkgLoaderResult


class WorkspacePkgLoaderResultTestCaseImpl(TestCase):
    _loader_result: Final[WorkspacePkgLoaderResult]

    def __init__(
        self,
        loader_result: WorkspacePkgLoaderResult
    ) -> None:
        self._loader_result = loader_result

    def run(self, test: Test) -> None:
        if (not self._loader_result.success()):
            raise UserException(self._loader_result.error())


class WorkspacePkgDirnameTestCaseImpl(TestCase):
    _pkg: Final[WorkspacePkg]

    def __init__(
        self,
        pkg: WorkspacePkg
    ) -> None:
        self._pkg = pkg

    def run(self, test: Test) -> None:
        sub_test = test.sub_test(
            msg=TextUtils.format_args(
                tpl="pkg dir {}",
                args=[
                    self._pkg.dirpath.absolute.path()
                ]
            )
        )

        with (sub_test):
            expected_dirpath = self._pkg.dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="../{}/",
                    args=[
                        self._pkg.name
                    ]
                )
            )

            test.assert_object_equal(
                expected=expected_dirpath.relative.path(),
                actual=self._pkg.dirpath.relative.path(),
                msg=lambda args: TextUtils.format(
                    tpl=(
                        "Invalid dirname\n"
                        "  e: [{expected}]\n"
                        "  a: [{actual}]"
                    ),
                    args=args
                )
            )


class WorkspacePkgSetupfileTestCaseImpl(FileContentTestCaseImpl):
    def __init__(
        self,
        template: JinjaTemplate,
        repo_url: str,
        pkg: WorkspacePkg
    ) -> None:
        super().__init__(
            filepath=pkg.setupfile_filepath.absolute,
            expected=template.render(
                name=pkg.name,
                repo_url=repo_url,
                package_data=CollectionUtils.to_dict(
                    items=CollectionUtils.map(
                        items=CollectionUtils.pairs(pkg.package_data),
                        fn=lambda pair: (pair.key, sorted(pair.value))
                    )
                ),
                package_dir=pkg.package_dir,
                entry_points=CollectionUtils.to_dict(
                    items=CollectionUtils.map(
                        items=CollectionUtils.pairs(pkg.entry_points),
                        fn=lambda pair: (pair.key, sorted(pair.value))
                    )
                ),
                scripts=sorted(pkg.scripts)
            )
        )


class WorkspacePkgCfgTestCaseImpl(TestCase):
    _pkg: Final[WorkspacePkg]

    def __init__(
        self,
        pkg: WorkspacePkg
    ) -> None:
        self._pkg = pkg

    def run(self, test: Test) -> None:
        for index, item in CollectionUtils.items(self._pkg.scripts):
            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="scripts.{} = {}",
                    args=[
                        index,
                        item
                    ]
                )
            )

            with (sub_test):
                PathUtils.raise_exception_on_unnormalized_file_path(item)


__all__: FinalCollection[str] = [
    "WorkspacePkgCfgTestCaseImpl",
    "WorkspacePkgDirnameTestCaseImpl",
    "WorkspacePkgLoaderResultTestCaseImpl",
    "WorkspacePkgSetupfileTestCaseImpl"
]
