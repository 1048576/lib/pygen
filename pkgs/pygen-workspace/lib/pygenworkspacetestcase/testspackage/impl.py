from __future__ import annotations

from collections.abc import Iterable

from pygenjinja.abc import JinjaTemplate
from pygenpy.abc import PyPackage
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDirpath


class WorkspaceTestsPackageInitModuleTestCaseImpl(FileContentTestCaseImpl):
    def __init__(
        self,
        template: JinjaTemplate,
        package: PyPackage
    ) -> None:
        super().__init__(
            filepath=package.init_module_filepath,
            expected=template.render(
                name=package.name
            )
        )


class WorkspaceTestsPackageTestModuleTestCaseImpl(FileContentTestCaseImpl):
    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath,
        repo_url: str,
        imports: Iterable[str],
        workspaces: Iterable[str]
    ) -> None:
        super().__init__(
            filepath=workspace_dirpath.absolute.resolve_filepath(
                path="./tests/test.py"
            ),
            expected=template.render(
                repo_url=repo_url,
                imports=sorted(imports),
                workspaces=sorted(workspaces)
            )
        )


__all__: FinalCollection[str] = [
    "WorkspaceTestsPackageInitModuleTestCaseImpl",
    "WorkspaceTestsPackageTestModuleTestCaseImpl"
]
