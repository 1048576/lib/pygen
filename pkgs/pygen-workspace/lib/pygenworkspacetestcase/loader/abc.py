from __future__ import annotations

from typing import Generic

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import T


class LoaderResult(Generic[T]):
    def error(self) -> str:
        raise SystemException()

    def success(self) -> bool:
        raise SystemException()

    def value(self) -> T:
        raise SystemException()


__all__: FinalCollection[str] = [
    "LoaderResult"
]
