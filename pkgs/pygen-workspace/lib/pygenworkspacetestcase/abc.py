from __future__ import annotations

from collections.abc import Iterable
from typing import Final
from typing import Protocol

from pygenerr.err import SystemException
from pygenos.abc import OSDir
from pygenpath.abc import Dirpath
from pygenpath.abc import PathT
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspacePkg
from pygenworkspace.abc import WorkspacePkgsDir
from pygenworkspace.abc import WorkspaceStubsDir
from pygenworkspace.abc import WorkspaceTestsDir
from pygenworkspacetestcase.loader.abc import LoaderResult


class WorkspacePkgLoaderResult(LoaderResult[WorkspacePkg]):
    ...


class WorkspacePkgLoader(object):
    def load(
        self,
        dirpath: Dirpath,
        dirs: Iterable[OSDir]
    ) -> WorkspacePkgLoaderResult:
        raise SystemException()


class WorkspacePkgsDirLoaderResult(LoaderResult[WorkspacePkgsDir]):
    ...


class WorkspacePkgsDirLoader(object):
    def load(self) -> WorkspacePkgsDirLoaderResult:
        raise SystemException()


class WorkspaceStubsDirLoaderResult(LoaderResult[WorkspaceStubsDir]):
    ...


class WorkspaceStubsDirLoader(object):
    def load(self) -> WorkspaceStubsDirLoaderResult:
        raise SystemException()


class WorkspaceTestsDirLoaderResult(LoaderResult[WorkspaceTestsDir]):
    ...


class WorkspaceTestsDirLoader(object):
    def load(self) -> WorkspaceTestsDirLoaderResult:
        raise SystemException()


class WorkspaceTestCaseConstants(object):
    PKGS_DIRNAME: Final[str] = "pkgs"
    STUBS_DIRNAME: Final[str] = "stubs"
    TESTS_DIRNAME: Final[str] = "tests"

    IMAGE_DIR_PATH: Final[str] = "./images/tests/"

    COVERAGE_CFG_PATH: Final[str] = "./.coveragerc"
    FLAKE8_CFG_PATH: Final[str] = "./.flake8"
    ISORT_CFG_PATH: Final[str] = "./.isort.cfg"
    PYRE_CFG_PATH: Final[str] = "./.pyre_configuration"
    PYRIGHT_CFG_PATH: Final[str] = "./pyrightconfig.json"
    REQUIREMENTS_PATH: Final[str] = "./.requirements.txt"


class WorkspaceTestCasePathMapper(Protocol):
    workspace_dirpath: Final[Dirpath]

    def to_relative_path(self, path: PathT, prefix: str = ...) -> str:
        raise SystemException()


class WorkspaceTestCaseLoader(object):
    def load(
        self,
        test_suite: TestSuite,
        tests_dirpath: Dirpath,
        pkgs: Iterable[WorkspacePkg]
    ) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "WorkspacePkgLoader",
    "WorkspacePkgLoaderResult",
    "WorkspacePkgsDirLoader",
    "WorkspacePkgsDirLoaderResult",
    "WorkspaceStubsDirLoader",
    "WorkspaceStubsDirLoaderResult",
    "WorkspaceTestCaseConstants",
    "WorkspaceTestCaseLoader",
    "WorkspaceTestCasePathMapper",
    "WorkspaceTestsDirLoader",
    "WorkspaceTestsDirLoaderResult"
]
