from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygenpy.abc import PyPackage
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class WorkspaceTestPackageModuleNameTestCaseImpl(TestCase):
    _packages: FinalCollection[PyPackage]

    def __init__(
        self,
        packages: Iterable[PyPackage]
    ) -> None:
        self._packages = ArrayImpl.copy(packages)

    def run(self, test: Test) -> None:
        for package in self._packages:
            for module in package.modules:
                module_path = TextUtils.join(
                    delimiter=".",
                    items=[
                        package.name,
                        module.name
                    ]
                )

                with (test.sub_test(module_path)):
                    test.assert_object_equal(
                        expected="test",
                        actual=module.name,
                        msg=lambda args: TextUtils.format_args(
                            tpl=(
                                "Expected module name [{}] != actual [{}]\n"
                                "  module path: {}\n"
                                "  package dirpath: {}\n"
                                "  module filepath: {}"
                            ),
                            args=[
                                args.expected,
                                args.actual,
                                module_path,
                                package.dirpath.path(),
                                module.filepath
                            ]
                        )
                    )


__all__: FinalCollection[str] = [
    "WorkspaceTestPackageModuleNameTestCaseImpl"
]
