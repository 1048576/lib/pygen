from __future__ import annotations

from collections.abc import Iterable
from typing import Final

import pygenworkspacetemplate

from pygencollection.impl import ArrayImpl
from pygencollection.impl import FrozenSetImpl
from pygenjinja.abc import JinjaTemplate
from pygenjinja.impl import JinjaTemplateLoaderImpl
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygenworkspace.abc import Workspace
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceFilepath
from pygenworkspace.abc import WorkspacePkgsDirDataCollector
from pygenworkspace.abc import WorkspaceStubsDirDataCollector
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext
from pygenworkspace.abc import WorkspaceTestsDirDataCollector
from pygenworkspace.datacollector.pkgsdir.impl import WorkspacePkgsDirDataCollectorImpl
from pygenworkspace.datacollector.stubsdir.impl import WorkspaceStubsDirDataCollectorImpl
from pygenworkspace.datacollector.testsdir.impl import WorkspaceTestsDirDataCollectorImpl
from pygenworkspace.testcase.coverage.impl import WorkspaceCoverageTestCaseLoaderImpl
from pygenworkspace.testcase.flake8.impl import WorkspaceFlake8TestCaseLoaderImpl
from pygenworkspace.testcase.image.impl import WorkspaceImageDockerfileTestCaseLoaderImpl
from pygenworkspace.testcase.isort.impl import WorkspaceIsortTestCaseLoaderImpl
from pygenworkspace.testcase.packagelink.impl import WorkspacePackageLinkTestCaseLoaderImpl
from pygenworkspace.testcase.pkgpackage.impl import WorkspacePkgPackageTestCaseLoaderImpl
from pygenworkspace.testcase.pkgsdir.impl import WorkspacePkgsDirTestCaseLoaderImpl
from pygenworkspace.testcase.pyre.impl import WorkspacePyreTestCaseLoaderImpl
from pygenworkspace.testcase.pyright.impl import WorkspacePyrightTestCaseLoaderImpl
from pygenworkspace.testcase.testsdir.impl import WorkspaceTestsDirTestCaseLoaderImpl


class WorkspaceDirpathImpl(WorkspaceDirpath):
    def resolve_dirpath(self, path: str) -> WorkspaceDirpath:
        return WorkspaceDirpathImpl(
            absolute=self.absolute.resolve_dirpath(path),
            relative=self.relative.resolve_dirpath(path)
        )

    def resolve_filepath(self, path: str) -> WorkspaceFilepath:
        return WorkspaceFilepath(
            absolute=self.absolute.resolve_filepath(path),
            relative=self.relative.resolve_filepath(path)
        )


class WorkspaceImpl(Workspace):
    _dirnames: FinalCollection[str]
    _entrypoint_template: Final[JinjaTemplate]
    _pkgs_dir_data_collector: Final[WorkspacePkgsDirDataCollector]
    _stubs_dir_data_collector: Final[WorkspaceStubsDirDataCollector]
    _tests_dir_data_collector: Final[WorkspaceTestsDirDataCollector]
    _loaders: FinalCollection[WorkspaceTestCaseLoader]

    def __init__(
        self,
        workspace_dirpath: WorkspaceDirpath,
        repo_url: str,
        dirnames: Iterable[str]
    ) -> None:
        template_loader = JinjaTemplateLoaderImpl()

        self._dirnames = ArrayImpl.copy(
            instance=sorted(FrozenSetImpl.copy(dirnames))
        )
        self._entrypoint_template = template_loader.load(
            module=pygenworkspacetemplate,
            name="entrypoint",
            keep_trailing_newline=False
        )
        self._pkgs_dir_data_collector = WorkspacePkgsDirDataCollectorImpl(
            workspace_dirpath=workspace_dirpath,
            dirnames=dirnames
        )
        self._stubs_dir_data_collector = WorkspaceStubsDirDataCollectorImpl(
            workspace_dirpath=workspace_dirpath,
            dirnames=dirnames
        )
        self._tests_dir_data_collector = WorkspaceTestsDirDataCollectorImpl(
            workspace_dirpath=workspace_dirpath
        )
        self._loaders = ArrayImpl.of(
            WorkspaceImageDockerfileTestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="image-dockerfile"
                ),
                workspace_dirpath=workspace_dirpath,
                dirnames=self._dirnames
            ),
            WorkspaceCoverageTestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="coverage-cfg"
                ),
                workspace_dirpath=workspace_dirpath
            ),
            WorkspaceFlake8TestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="flake8-cfg"
                ),
                workspace_dirpath=workspace_dirpath
            ),
            WorkspaceIsortTestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="isort-cfg"
                ),
                workspace_dirpath=workspace_dirpath
            ),
            WorkspacePyreTestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="pyre-cfg"
                ),
                workspace_dirpath=workspace_dirpath
            ),
            WorkspacePyrightTestCaseLoaderImpl(
                template=template_loader.load(
                    module=pygenworkspacetemplate,
                    name="pyright-cfg"
                ),
                workspace_dirpath=workspace_dirpath
            ),
            WorkspacePkgsDirTestCaseLoaderImpl(),
            WorkspaceTestsDirTestCaseLoaderImpl(),
            WorkspacePkgPackageTestCaseLoaderImpl(),
            WorkspacePackageLinkTestCaseLoaderImpl()
        )

    def entrypoint(self) -> str:
        return self._entrypoint_template.render(
            dirnames=sorted(self._dirnames)
        )

    def load_tests(self, test_suite: TestSuite) -> None:
        context = WorkspaceTestCaseLoaderContext(
            pkgs_dir=self._pkgs_dir_data_collector.collect(),
            stubs_dir=self._stubs_dir_data_collector.collect(),
            tests_dir=self._tests_dir_data_collector.collect()
        )

        for loader in self._loaders:
            loader.load(test_suite, context)


__all__: FinalCollection[str] = [
    "WorkspaceDirpathImpl",
    "WorkspaceImpl"
]
