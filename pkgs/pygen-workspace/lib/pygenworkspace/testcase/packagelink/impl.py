from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenpy.abc import PyPackage
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentest.abc import TestSuite
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspacePackageLink
from pygenworkspace.abc import WorkspacePackageLinkOwner
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspacePackageLinkTestCaseImpl(TestCase):
    _packages: FinalCollection[PyPackage]
    _links: FinalCollection[WorkspacePackageLink]

    def __init__(
        self,
        packages: Iterable[PyPackage],
        links: Iterable[WorkspacePackageLink]
    ) -> None:
        self._packages = ArrayImpl.copy(packages)
        self._links = ArrayImpl.copy(links)

    def run(self, test: Test) -> None:
        for link in self._links:
            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="{} -> {}",
                    args=[
                        link.owner.name,
                        link.reference
                    ]
                )
            )

            with (sub_test):
                actual = CollectionUtils.any(
                    items=self._packages,
                    fn=lambda package: (package.name == link.reference)
                )

                test.assert_scalar_equal(
                    expected=True,
                    actual=actual,
                    msg=lambda args: TextUtils.format_args(
                        tpl=(
                            "Package [{}] not found:\n"
                            "  owner package name: {}\n"
                            "  owner package dirpath: {}\n"
                            "  possible causes of the problem:\n"
                            "    - absent __init__.py\n"
                            "    - problems in setup.py\n"
                            "    - package removed or renamed"
                        ),
                        args=[
                            link.reference,
                            link.owner.name,
                            link.owner.dirpath.path()
                        ]
                    )
                )


class WorkspacePackageLinkTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_package_name_prefix = TextUtils.format_args(
            tpl="{}.{}.",
            args=[
                context.tests_dir.dir.dirname(),
                context.pkgs_dir.dir.dirname()
            ]
        )

        pkg_packages = ArrayImpl.collect(
            iterator=CollectionUtils.chain(
                links=CollectionUtils.map(
                    items=context.pkgs_dir.pkgs,
                    fn=lambda pkgs: pkgs.packages
                )
            )
        )

        pkg_links = ArrayImpl.collect(
            iterator=CollectionUtils.map(
                items=pkg_packages,
                fn=lambda package: WorkspacePackageLink(
                    owner=WorkspacePackageLinkOwner(
                        name=package.name,
                        dirpath=package.dirpath
                    ),
                    reference=TextUtils.format_args(
                        tpl="{}{}",
                        args=[
                            test_package_name_prefix,
                            package.name
                        ]
                    )
                )
            )
        )

        test_packages = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=context.tests_dir.packages,
                fn=lambda package: TextUtils.startswith(
                    text=package.name,
                    prefix=test_package_name_prefix
                )
            )
        )

        test_links = ArrayImpl.collect(
            iterator=CollectionUtils.map(
                items=test_packages,
                fn=lambda package: WorkspacePackageLink(
                    owner=WorkspacePackageLinkOwner(
                        name=package.name,
                        dirpath=package.dirpath
                    ),
                    reference=package.name[len(test_package_name_prefix):]
                )
            )
        )

        test_suite.add_test_case(
            test_case=WorkspacePackageLinkTestCaseImpl(
                packages=test_packages,
                links=pkg_links
            )
        )

        test_suite.add_test_case(
            test_case=WorkspacePackageLinkTestCaseImpl(
                packages=pkg_packages,
                links=test_links
            )
        )


__all__: FinalCollection[str] = [
    "WorkspacePackageLinkTestCaseLoaderImpl"
]
