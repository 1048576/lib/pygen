from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygenjinja.abc import JinjaTemplate
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspacePyreCfgTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspacePyreTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]
    _workspace_dirpath: Final[WorkspaceDirpath]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        self._template = template
        self._workspace_dirpath = workspace_dirpath

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        exclude = self._exclude(context)
        source_directories = self._source_directories(context)
        search_paths = self._search_paths(context)

        test_suite.add_test_case(
            test_case=WorkspacePyreCfgTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path="./.pyre_configuration"
                ),
                expected=self._template.render(
                    exclude=sorted(exclude),
                    source_directories=sorted(source_directories),
                    search_paths=sorted(search_paths)
                )
            )
        )

    def _exclude(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                yield TextUtils.format_args(
                    tpl=".*{}.*",
                    args=[
                        pkg.build_dirpath.relative.path()[1:]
                    ]
                )

    def _search_paths(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                for dirname in pkg.package_dir.values():
                    dirpath = pkg.dirpath.relative.resolve_dirpath(
                        path=TextUtils.format_args(
                            tpl="./{}/",
                            args=[
                                dirname
                            ]
                        )
                    )

                    yield dirpath.path()

        if (context.stubs_dir.used):
            yield context.stubs_dir.dir.dirpath().relative.path()

    def _source_directories(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            yield context.pkgs_dir.dir.dirpath().relative.path()

        if (context.stubs_dir.used):
            yield context.stubs_dir.dir.dirpath().relative.path()

        yield context.tests_dir.dir.dirpath().relative.path()


__all__: FinalCollection[str] = [
    "WorkspacePyreTestCaseLoaderImpl"
]
