from __future__ import annotations

from typing import Final

from pygenjinja.abc import JinjaTemplate
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceConstants
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspaceIsortCfgTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspaceIsortTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]
    _workspace_dirpath: Final[WorkspaceDirpath]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        self._template = template
        self._workspace_dirpath = workspace_dirpath

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_suite.add_test_case(
            test_case=WorkspaceIsortCfgTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path=WorkspaceConstants.ISORT_CFG_PATH
                ),
                expected=self._template.render()
            )
        )


__all__: FinalCollection[str] = [
    "WorkspaceIsortTestCaseLoaderImpl"
]
