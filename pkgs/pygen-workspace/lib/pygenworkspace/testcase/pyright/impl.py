from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygenjinja.abc import JinjaTemplate
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygenworkspace.abc import WorkspaceConstants
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspacePyrightCfgTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspacePyrightTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        self._template = template
        self._workspace_dirpath = workspace_dirpath

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        exclude = self._exclude(context)
        extra_paths = self._extra_paths(context)
        include = self._include(context)
        stub_path = self._stub_path(context)

        test_suite.add_test_case(
            test_case=WorkspacePyrightCfgTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path=WorkspaceConstants.PYRIGHT_CFG_PATH
                ),
                expected=self._template.render(
                    exclude=sorted(exclude),
                    extra_paths=sorted(extra_paths),
                    include=sorted(include),
                    stub_path=stub_path
                )
            )
        )

    def _exclude(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                yield TextUtils.format_args(
                    tpl="{}**/*",
                    args=[
                        pkg.build_dirpath.relative.path()
                    ]
                )

    def _extra_paths(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                for dirname in pkg.package_dir.values():
                    dirpath = pkg.dirpath.relative.resolve_dirpath(
                        path=TextUtils.format_args(
                            tpl="./{}/",
                            args=[
                                dirname
                            ]
                        )
                    )

                    yield dirpath.path()

    def _include(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            yield context.pkgs_dir.dir.dirpath().relative.path()

        if (context.stubs_dir.used):
            yield context.stubs_dir.dir.dirpath().relative.path()

        yield context.tests_dir.dir.dirpath().relative.path()

    def _stub_path(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Optional[str]:
        if (context.stubs_dir.used):
            return context.stubs_dir.dir.dirpath().relative.path()
        else:
            return None


__all__: FinalCollection[str] = [
    "WorkspacePyrightTestCaseLoaderImpl"
]
