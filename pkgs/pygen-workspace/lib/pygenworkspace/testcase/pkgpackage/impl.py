from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenpy.abc import PyPackage
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentest.abc import TestSuite
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspacePkgPackageModuleNameTestCaseImpl(TestCase):
    _packages: FinalCollection[PyPackage]

    def __init__(
        self,
        packages: Iterable[PyPackage]
    ) -> None:
        self._packages = ArrayImpl.copy(packages)

    def run(self, test: Test) -> None:
        expected = ArrayImpl.of(
            "abc",
            "err",
            "impl",
            "utils"
        )

        for package in self._packages:
            for module in package.modules:
                module_path = TextUtils.join(
                    delimiter=".",
                    items=[
                        package.name,
                        module.name
                    ]
                )

                with (test.sub_test(module_path)):
                    test.assert_scalar_equal(
                        expected=True,
                        actual=(module.name in expected),
                        msg=lambda args: TextUtils.format_args(
                            tpl=(
                                "Actual module name [{}] not in expected {}\n"
                                "  module path: {}\n"
                                "  package dirpath: {}\n"
                                "  module filepath: {}"
                            ),
                            args=[
                                module.name,
                                expected,
                                module_path,
                                package.dirpath.path(),
                                module.filepath
                            ]
                        )
                    )


class WorkspacePkgPackageTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_suite.add_test_case(
            test_case=WorkspacePkgPackageModuleNameTestCaseImpl(
                packages=ArrayImpl.collect(
                    iterator=CollectionUtils.chain(
                        links=CollectionUtils.map(
                            items=context.pkgs_dir.pkgs,
                            fn=lambda pkg: pkg.packages
                        )
                    )
                )
            )
        )


__all__: FinalCollection[str] = [
    "WorkspacePkgPackageTestCaseLoaderImpl"
]
