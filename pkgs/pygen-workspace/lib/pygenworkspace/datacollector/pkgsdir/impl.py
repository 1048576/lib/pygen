from __future__ import annotations

import os

from collections.abc import Iterable
from collections.abc import Iterator
from typing import Final

from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygenerr.err import UserException
from pygenfn.impl import ProcedureInstance
from pygenos.abc import OSDir
from pygenos.impl import OSDirImpl
from pygenos.utils import OSUtils
from pygenpy.utils import PyUtils
from pygenpypkgsetupfile.utils import PyPkgSetupfileUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDir
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspacePkg
from pygenworkspace.abc import WorkspacePkgEgg
from pygenworkspace.abc import WorkspacePkgsDir
from pygenworkspace.abc import WorkspacePkgsDirDataCollector


class WorkspacePkgsDirDataCollectorImpl(WorkspacePkgsDirDataCollector):
    _workspace_dirpath: Final[WorkspaceDirpath]
    _used: Final[bool]
    _dir: Final[WorkspaceDir]

    def __init__(
        self,
        workspace_dirpath: WorkspaceDirpath,
        dirnames: Iterable[str]
    ) -> None:
        dirname = "pkgs"
        used = (dirname in dirnames)

        if (used):
            dirpath = workspace_dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        dirname
                    ]
                )
            )
            dir = WorkspaceDir(
                dirname=lambda: dirname,
                dirpath=lambda: dirpath
            )
        else:
            dir = WorkspaceDir(
                dirname=ProcedureInstance(),
                dirpath=ProcedureInstance()
            )

        self._workspace_dirpath = workspace_dirpath
        self._used = used
        self._dir = dir

    def collect(self) -> WorkspacePkgsDir:
        if (not self._used):
            return WorkspacePkgsDir(
                used=False,
                dir=self._dir,
                dirs=ArrayImpl.of(),
                pkgs=ArrayImpl.of()
            )
        elif (os.path.exists(self._dir.dirpath().absolute.path())):
            dirs = ArrayImpl.copy(
                instance=self._find_dirs()
            )

            pkgs = ArrayImpl.copy(
                instance=self._find_pkgs(dirs)
            )

            return WorkspacePkgsDir(
                used=self._used,
                dir=self._dir,
                dirs=dirs,
                pkgs=pkgs
            )
        else:
            raise UserException(
                msg=TextUtils.format_args(
                    tpl="Pkgs dir doesn't exist [{}]",
                    args=[
                        self._dir.dirpath().absolute.path()
                    ]
                )
            )

    def _create_pkg(
        self,
        dirs: Array[OSDir],
        dirpath: WorkspaceDirpath
    ) -> WorkspacePkg:
        setupfile_filepath = dirpath.resolve_filepath("./setup.py")

        setupfile = PyPkgSetupfileUtils.parse(setupfile_filepath.absolute)

        build_dirpath = dirpath.resolve_dirpath("./build/")

        lib_dirpath = dirpath.resolve_dirpath("./lib/")

        requirements_filepath = dirpath.resolve_filepath(
            path="./requirements.txt"
        )

        packages = ArrayImpl.collect(
            iterator=PyUtils.find_packages(
                dirs=dirs,
                dirpath=lib_dirpath.absolute
            )
        )

        egg = WorkspacePkgEgg(
            info_dirpath=lib_dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./{}.egg-info/",
                    args=[
                        setupfile.name.replace("-", "_")
                    ]
                )
            )
        )

        return WorkspacePkg(
            dirpath=dirpath,
            build_dirpath=build_dirpath,
            lib_dirpath=lib_dirpath,
            setupfile_filepath=setupfile_filepath,
            requirements_filepath=requirements_filepath,
            name=setupfile.name,
            package_data=setupfile.package_data,
            package_dir=setupfile.package_dir,
            entry_points=setupfile.entry_points,
            packages=packages,
            scripts=setupfile.scripts,
            egg=egg
        )

    def _find_dirs(self) -> Iterator[OSDir]:
        yield OSDirImpl(
            dirpath=self._workspace_dirpath.absolute,
            dirnames=ArrayImpl.of(self._dir.dirname()),
            filenames=ArrayImpl.of()
        )

        for dirpath in OSUtils.walk(self._dir.dirpath().absolute):
            yield dirpath

    def _find_pkgs(self, dirs: Array[OSDir]) -> Iterator[WorkspacePkg]:
        for dir in dirs:
            if (dir.dirpath == self._dir.dirpath().absolute):
                break
        else:
            return

        for dirname in dir.dirnames:
            yield self._create_pkg(
                dirs=dirs,
                dirpath=self._dir.dirpath().resolve_dirpath(
                    path=TextUtils.format_args(
                        tpl="./{}/",
                        args=[
                            dirname
                        ]
                    )
                )
            )


__all__: FinalCollection[str] = [
    "WorkspacePkgsDirDataCollectorImpl"
]
