from __future__ import annotations

import os.path

from collections.abc import Iterable
from typing import Final

from pygenerr.err import UserException
from pygenfn.impl import ProcedureInstance
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDir
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceStubsDir
from pygenworkspace.abc import WorkspaceStubsDirDataCollector


class WorkspaceStubsDirDataCollectorImpl(WorkspaceStubsDirDataCollector):
    _used: Final[bool]
    _dir: Final[WorkspaceDir]

    def __init__(
        self,
        workspace_dirpath: WorkspaceDirpath,
        dirnames: Iterable[str]
    ) -> None:
        dirname = "stubs"
        used = (dirname in dirnames)

        if (used):
            dirpath = workspace_dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        dirname
                    ]
                )
            )
            dir = WorkspaceDir(
                dirname=lambda: dirname,
                dirpath=lambda: dirpath
            )
        else:
            dir = WorkspaceDir(
                dirname=ProcedureInstance(),
                dirpath=ProcedureInstance()
            )

        self._used = used
        self._dir = dir

    def collect(self) -> WorkspaceStubsDir:
        if (not self._used):
            return WorkspaceStubsDir(
                used=self._used,
                dir=self._dir
            )
        elif (os.path.exists(self._dir.dirpath().absolute.path())):
            return WorkspaceStubsDir(
                used=self._used,
                dir=self._dir
            )
        else:
            raise UserException(
                msg=TextUtils.format_args(
                    tpl="Stubs dir doesn't exist [{}]",
                    args=[
                        self._dir.dirpath().absolute.path()
                    ]
                )
            )


__all__: FinalCollection[str] = [
    "WorkspaceStubsDirDataCollectorImpl"
]
