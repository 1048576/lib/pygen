from __future__ import annotations

from pygenerr.err import SystemException
from pygentextdoc.abc import TextDocStream
from pygentype.abc import FinalCollection


class YamlJinjaDocLoader(object):
    def load_stream_from_file(self, path: str) -> TextDocStream:
        raise SystemException()


__all__: FinalCollection[str] = [
    "YamlJinjaDocLoader"
]
