from __future__ import annotations

from pygenerr.err import UserException
from pygentype.abc import FinalCollection


class AssertException(UserException):
    ...


__all__: FinalCollection[str] = [
    "AssertException"
]
