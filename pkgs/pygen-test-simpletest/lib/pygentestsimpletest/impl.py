from __future__ import annotations

from pygentest.abc import Test
from pygentest.abc import TestAssertObjectEqualMsg
from pygentest.abc import TestAssertObjectEqualMsgArgs
from pygentest.abc import TestAssertScalarEqualMsg
from pygentest.impl import TestAssertObjectEqualMsgImpl
from pygentest.impl import TestAssertScalarEqualMsgImpl
from pygentestsimpletest.err import AssertException
from pygentype.abc import FinalCollection
from pygentype.abc import ScalarT
from pygentype.utils import TypeUtils


class SimpleTestImpl(Test):
    def assert_object_equal(
        self,
        expected: object,
        actual: object,
        msg: TestAssertObjectEqualMsg = TestAssertObjectEqualMsgImpl()
    ) -> None:
        if (not TypeUtils.eq(expected, actual)):
            args = TestAssertObjectEqualMsgArgs(expected, actual)

            raise AssertException(msg(args))

    def assert_scalar_equal(
        self,
        expected: ScalarT,
        actual: ScalarT,
        msg: TestAssertScalarEqualMsg = TestAssertScalarEqualMsgImpl()
    ) -> None:
        self.assert_object_equal(expected, actual, msg)


__all__: FinalCollection[str] = [
    "SimpleTestImpl"
]
