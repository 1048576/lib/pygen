from __future__ import annotations

from pygenerr.err import UserException
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class TreeException(UserException):
    ...


class TreeInvalidNodeTypeException(TreeException):
    @classmethod
    def create(
        cls,
        path: str,
        expected_type: str,
        actual_type: str
    ) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl="Node [{}]: Invalid node type. [{}] instead of [{}]",
                args=[
                    path,
                    actual_type,
                    expected_type
                ]
            )
        )


class TreeNodeNotFoundException(TreeException):
    @classmethod
    def create(cls, path: str) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl="Node [{}] not found",
                args=[
                    path
                ]
            )
        )


class TreeUnexpectedNodesException(TreeException):
    @classmethod
    def create(cls, nodes: str) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl="Unexpected node(s) [{}]",
                args=[
                    nodes
                ]
            )
        )


__all__: FinalCollection[str] = [
    "TreeInvalidNodeTypeException",
    "TreeNodeNotFoundException",
    "TreeUnexpectedNodesException"
]
