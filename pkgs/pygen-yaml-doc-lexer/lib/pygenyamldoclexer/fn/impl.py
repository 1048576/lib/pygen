from __future__ import annotations

from typing import Final

from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerFnMapper
from pygenyamldoclexer.abc import YamlDocLexerNextFn
from pygenyamldoclexer.abc import YamlDocLexerProcessFn
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerSuccessFn
from pygenyamldoclexer.abc import YamlDocLexerValue
from pygenyamldoclexer.abc import YamlDocLexerValueFn


class YamlDocLexerNextFnImpl(YamlDocLexerNextFn):
    def __call__(
        self,
        result: YamlDocLexerResult
    ) -> YamlDocLexerProcessor:
        return result.next()


class YamlDocLexerProcessFnImpl(YamlDocLexerProcessFn):
    def __call__(
        self,
        processor: YamlDocLexerProcessor,
        reader: TextReader
    ) -> YamlDocLexerResult:
        return processor.process(reader)


class YamlDocLexerSuccessFnImpl(YamlDocLexerSuccessFn):
    def __call__(self, result: YamlDocLexerResult) -> bool:
        return result.success()


class YamlDocLexerValueFnImpl(YamlDocLexerValueFn):
    def __call__(
        self,
        result: YamlDocLexerResult
    ) -> YamlDocLexerValue:
        return result.value()


class YamlDocLexerFnMapperImpl(YamlDocLexerFnMapper):
    NEXT_FN: Final[YamlDocLexerNextFn] = YamlDocLexerNextFnImpl()
    PROCESS_FN: Final[YamlDocLexerProcessFn] = YamlDocLexerProcessFnImpl()
    VALUE_FN: Final[YamlDocLexerValueFn] = YamlDocLexerValueFnImpl()
    SUCCESS_FN: Final[YamlDocLexerSuccessFn] = YamlDocLexerSuccessFnImpl()

    def next_fn(self) -> YamlDocLexerNextFn:
        return self.NEXT_FN

    def process_fn(self) -> YamlDocLexerProcessFn:
        return self.PROCESS_FN

    def success_fn(self) -> YamlDocLexerSuccessFn:
        return self.SUCCESS_FN

    def value_fn(self) -> YamlDocLexerValueFn:
        return self.VALUE_FN


__all__: FinalCollection[str] = [
    "YamlDocLexerFnMapperImpl"
]
