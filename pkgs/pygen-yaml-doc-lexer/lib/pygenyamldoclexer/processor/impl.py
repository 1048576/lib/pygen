from __future__ import annotations

from collections.abc import Callable
from collections.abc import Iterable
from dataclasses import dataclass
from typing import Final

from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygenerr.err import SystemException
from pygentextdoclexer.processor.impl import TextDocLexerCacheProcessorImpl
from pygentextdoclexer.processor.impl import TextDocLexerForkProcessorImpl
from pygentextdoclexer.processor.impl import TextDocLexerLnProcessorImpl
from pygentextreader.abc import TextReader
from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerFnMapper
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValueMapper
from pygenyamldoclexer.processor.begindict.impl import YamlDocLexerBeginDictProcessorImpl
from pygenyamldoclexer.processor.beginlist.impl import YamlDocLexerBeginListProcessorImpl
from pygenyamldoclexer.processor.beginlistitem.impl import YamlDocLexerBeginListItemProcessorImpl
from pygenyamldoclexer.processor.dictkey.impl import YamlDocLexerDictKeyProcessorImpl
from pygenyamldoclexer.processor.document.impl import YamlDocLexerDocumentProcessorImpl
from pygenyamldoclexer.processor.textliteral.impl import YamlDocLexerTextLiteralProcessorImpl


class YamlDocLexerCacheProcessorImpl(
    TextDocLexerCacheProcessorImpl[YamlDocLexerResult],
    YamlDocLexerProcessor
):
    ...


class YamlDocLexerForkProcessorImpl(
    TextDocLexerForkProcessorImpl[
        YamlDocLexerProcessor,
        YamlDocLexerResult
    ],
    YamlDocLexerProcessor
):
    def __init__(
        self,
        fn_mapper: YamlDocLexerFnMapper,
        processors: Array[YamlDocLexerProcessor],
        on_failure_fn: Callable[[], YamlDocLexerResult]
    ) -> None:
        super().__init__(
            processors=processors,
            on_failure_fn=on_failure_fn,
            process_fn=fn_mapper.process_fn(),
            success_fn=fn_mapper.success_fn()
        )


class YamlDocLexerLnProcessorImpl(
    TextDocLexerLnProcessorImpl[YamlDocLexerResult],
    YamlDocLexerProcessor
):
    ...


class YamlDocLexerStubProcessorImpl(YamlDocLexerProcessor):
    def process(self, reader: TextReader) -> YamlDocLexerResult:
        raise SystemException()


@dataclass
class YamlDocLexerProcessorMapperImpl(YamlDocLexerProcessorMapper):
    result_mapper: Final[YamlDocLexerResultMapper]
    fn_mapper: Final[YamlDocLexerFnMapper]
    value_mapper: Final[YamlDocLexerValueMapper]

    def begin_dict(
        self,
        first_line_indent: int,
        other_line_indent: int,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerBeginDictProcessorImpl(
            result_mapper=self.result_mapper,
            processor_mapper=self,
            first_line_indent=first_line_indent,
            other_line_indent=other_line_indent,
            next=next
        )

    def begin_list(
        self,
        indent: int,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerBeginListProcessorImpl(
            result_mapper=self.result_mapper,
            processor_mapper=self,
            indent=indent,
            next=next
        )

    def begin_list_item(
        self,
        indent: int,
        end_list_item_processor: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerBeginListItemProcessorImpl(
            result_mapper=self.result_mapper,
            indent=indent,
            end_list_item_processor=end_list_item_processor
        )

    def dict_key(
        self,
        indent: int
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerDictKeyProcessorImpl(
            result_mapper=self.result_mapper,
            value_mapper=self.value_mapper,
            processor_mapper=self,
            indent=indent
        )

    def document(self) -> YamlDocLexerProcessor:
        return YamlDocLexerDocumentProcessorImpl(
            result_mapper=self.result_mapper,
            processor_mapper=self
        )

    def fork(
        self,
        processors: Iterable[YamlDocLexerProcessor],
        on_failure_fn: Callable[[], YamlDocLexerResult]
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerForkProcessorImpl(
            fn_mapper=self.fn_mapper,
            processors=ArrayImpl.copy(processors),
            on_failure_fn=on_failure_fn
        )

    def ln(
        self,
        on_success_fn: Callable[[], YamlDocLexerResult],
        on_failure_fn: Callable[[TextReaderPosition], YamlDocLexerResult]
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerLnProcessorImpl(
            on_success_fn=on_success_fn,
            on_failure_fn=on_failure_fn
        )

    def stub(self) -> YamlDocLexerProcessor:
        return YamlDocLexerStubProcessorImpl()

    def text_literal(
        self,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        return YamlDocLexerTextLiteralProcessorImpl(
            result_mapper=self.result_mapper,
            value_mapper=self.value_mapper,
            next=next
        )


__all__: FinalCollection[str] = [
    "YamlDocLexerProcessorMapperImpl"
]
