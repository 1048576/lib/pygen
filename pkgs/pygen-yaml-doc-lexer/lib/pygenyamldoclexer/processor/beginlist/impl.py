from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper


@dataclass
class YamlDocLexerBeginListProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    processor_mapper: Final[YamlDocLexerProcessorMapper]
    indent: Final[int]
    next: Final[YamlDocLexerProcessor]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        begin_list_item_processor = self.processor_mapper.begin_list_item(
            indent=self.indent,
            end_list_item_processor=self.processor_mapper.stub()
        )

        begin_list_item_processor_result = begin_list_item_processor.process(
            reader=reader
        )

        if (begin_list_item_processor_result.success()):
            raise SystemException()
        else:
            return self.result_mapper.failure(
                code="Y003",
                position=reader.position()
            )


__all__: FinalCollection[str] = [
    "YamlDocLexerBeginListProcessorImpl"
]
