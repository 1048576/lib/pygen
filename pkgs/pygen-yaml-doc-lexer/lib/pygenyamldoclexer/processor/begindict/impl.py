from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper


@dataclass
class YamlDocLexerBeginDictProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    processor_mapper: Final[YamlDocLexerProcessorMapper]
    first_line_indent: Final[int]
    other_line_indent: Final[int]
    next: Final[YamlDocLexerProcessor]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        dict_key_processor = self.processor_mapper.dict_key(
            indent=self.first_line_indent
        )

        dict_key_processor_result = dict_key_processor.process(reader)

        if (dict_key_processor_result.success()):
            raise SystemException()
        else:
            return self.result_mapper.failure(
                code="Y002",
                position=reader.position()
            )


__all__: FinalCollection[str] = [
    "YamlDocLexerBeginDictProcessorImpl"
]
