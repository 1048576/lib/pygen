from __future__ import annotations

from dataclasses import dataclass
from io import StringIO
from typing import Final

from pygenalphabet.utils import AlphabetUtils
from pygencounter.impl import CounterImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerConstants
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


@dataclass
class YamlDocLexerDictKeyProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    value_mapper: Final[YamlDocLexerValueMapper]
    processor_mapper: Final[YamlDocLexerProcessorMapper]
    indent: Final[int]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()
        value = StringIO()

        for _ in range(0, self.indent):
            for _ in range(0, YamlDocLexerConstants.INDENT_LENGTH):
                if (reader.peek(index.inc()) != " "):
                    return self.result_mapper.failure(
                        code="DictKey000",
                        position=reader.peek_position(index.value() - 1)
                    )

        char = reader.peek(index.inc())

        if (char == ""):
            return self.result_mapper.failure(
                code="DictKey001",
                position=reader.peek_position(index.value() - 1)
            )
        elif (char not in AlphabetUtils.LATIN):
            return self.result_mapper.failure(
                code="DictKey002",
                position=reader.peek_position(index.value() - 1)
            )
        else:
            value.write(char)

        while (True):
            char = reader.peek(index.inc())

            if (char == ":"):
                break
            elif (char == ""):
                return self.result_mapper.failure(
                    code="DictKey003",
                    position=reader.peek_position(index.value() - 1)
                )

            while (True):
                if (char in AlphabetUtils.LATIN):
                    break
                elif (char in AlphabetUtils.NUMBERS):
                    break
                elif (char in (".", "/", "_", "-")):
                    break

                return self.result_mapper.failure(
                    code="DictKey004",
                    position=reader.peek_position(index.value() - 1)
                )

            value.write(char)

        char = reader.peek(index.value())

        if (char in (" ", "\n")):
            reader.forward(index.value())

            return self.result_mapper.success(
                value=self.value_mapper.dict_key(
                    value=value.getvalue()
                ),
                next=self.processor_mapper.stub()
            )
        else:
            return self.result_mapper.failure(
                code="DictKey005",
                position=reader.peek_position(index.value() - 1)
            )


__all__: FinalCollection[str] = [
    "YamlDocLexerDictKeyProcessorImpl"
]
