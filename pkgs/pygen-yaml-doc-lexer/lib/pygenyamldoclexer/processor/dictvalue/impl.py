from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencounter.impl import CounterImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


@dataclass
class YamlDocLexerDictValueProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    value_mapper: Final[YamlDocLexerValueMapper]
    processor_mapper: Final[YamlDocLexerProcessorMapper]
    end_dict_processor: Final[YamlDocLexerProcessor]
    indent: Final[int]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()

        char = reader.peek(index.inc())

        if (char == " "):
            reader.forward(index.value())

            literal_processor = self.processor_mapper.fork(
                processors=[
                    self.processor_mapper.text_literal(
                        next=self.end_dict_processor
                    )
                ],
                on_failure_fn=lambda:
                    self.result_mapper.failure(
                        code="DictValue000",
                        position=reader.peek_position(index.value() - 1)
                    )
            )

            literal_processor_result = literal_processor.process(reader)

            if (not literal_processor_result.success()):
                return literal_processor_result
            else:
                ln_processor = self.processor_mapper.ln(
                    on_success_fn=lambda: literal_processor_result,
                    on_failure_fn=lambda position:
                        self.result_mapper.failure(
                            code="DictValue001",
                            position=position
                        )
                )

                return ln_processor.process(reader)
        elif (char == "\n"):
            reader.forward(index.value())

            container_processor = self.processor_mapper.fork(
                processors=[
                    self.processor_mapper.begin_dict(
                        first_line_indent=(self.indent + 1),
                        other_line_indent=(self.indent + 1),
                        next=self.end_dict_processor
                    ),
                    self.processor_mapper.begin_list(
                        indent=(self.indent + 1),
                        next=self.end_dict_processor
                    )
                ],
                on_failure_fn=lambda:
                    self.result_mapper.failure(
                        code="DictValue002",
                        position=reader.peek_position(index.value())
                    )
            )

            return container_processor.process(reader)
        else:
            return self.result_mapper.failure(
                code="DictValue003",
                position=reader.peek_position(index.value())
            )


__all__: FinalCollection[str] = [
    "YamlDocLexerDictValueProcessorImpl"
]
