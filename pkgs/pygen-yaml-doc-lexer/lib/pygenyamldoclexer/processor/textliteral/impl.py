from __future__ import annotations

from dataclasses import dataclass
from io import StringIO
from typing import Final

from pygencounter.impl import CounterImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerConstants
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


@dataclass
class YamlDocLexerTextLiteralProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    value_mapper: Final[YamlDocLexerValueMapper]
    next: Final[YamlDocLexerProcessor]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()
        value = StringIO()

        if (reader.peek(index.inc()) != "\""):
            return self.result_mapper.failure(
                code="TextLiteral000",
                position=reader.peek_position(index.value() - 1)
            )

        while (True):
            char = reader.peek(index.inc())

            if (char == ""):
                return self.result_mapper.failure(
                    code="TextLiteral001",
                    position=reader.peek_position(index.value() - 1)
                )
            elif (char == "\""):
                value = self.value_mapper.text_literal(
                    code="TextLiteral",
                    value=value.getvalue(),
                    begin_position=reader.position(),
                    end_position=reader.peek_position(index.value())
                )

                reader.forward(index.value())

                return self.result_mapper.success(
                    value=value,
                    next=self.next
                )
            elif (char == "\\"):
                next_char = reader.peek(index.inc())

                if (next_char in YamlDocLexerConstants.SPECIAL_CHARS):
                    value.write(char)
                    value.write(next_char)
                else:
                    return self.result_mapper.failure(
                        code="TextLiteral002",
                        position=reader.peek_position(index.value() - 1)
                    )
            else:
                value.write(char)


__all__: FinalCollection[str] = [
    "YamlDocLexerTextLiteralProcessorImpl"
]
