from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencounter.impl import CounterImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper


@dataclass
class YamlDocLexerDocumentProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    processor_mapper: Final[YamlDocLexerProcessorMapper]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()

        for char in "---\n":
            if (char != reader.peek(index.inc())):
                return self.result_mapper.failure(
                    code="Y000",
                    position=reader.peek_position(index.value() - 1)
                )

        reader.forward(index.value())

        processor = self.processor_mapper.fork(
            processors=[
                self.processor_mapper.begin_dict(
                    first_line_indent=0,
                    other_line_indent=0,
                    next=self.processor_mapper.stub()
                ),
                self.processor_mapper.begin_list(
                    indent=0,
                    next=self.processor_mapper.stub()
                )
            ],
            on_failure_fn=lambda:
                self.result_mapper.failure(
                    code="Y001",
                    position=reader.peek_position(index.value())
                )
        )

        return processor.process(reader)


__all__: FinalCollection[str] = [
    "YamlDocLexerDocumentProcessorImpl"
]
