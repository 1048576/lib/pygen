from __future__ import annotations

from typing import Generic
from typing import TypeVar

from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseArgsFn
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseEntrypointFn
from pygentype.abc import FinalCollection
from pygenyamldoclexerdev.abc import YamlDocLexerValueCollection

ArgsT = TypeVar("ArgsT")


class YamlDocLexerTestCaseArgsFn(
    Generic[ArgsT],
    TextDocLexerTestCaseArgsFn[ArgsT]
):
    ...


class YamlDocLexerTestCaseEntrypointFn(
    Generic[ArgsT],
    TextDocLexerTestCaseEntrypointFn[ArgsT, YamlDocLexerValueCollection]
):
    ...


__all__: FinalCollection[str] = [
    "YamlDocLexerTestCaseArgsFn",
    "YamlDocLexerTestCaseEntrypointFn"
]
