from __future__ import annotations

from pygencollection.abc import Array
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerValue


class YamlDocLexerValueCollection(Array[YamlDocLexerValue]):
    ...


class YamlDocLexerValueList(YamlDocLexerValueCollection):
    def add(self, value: YamlDocLexerValue) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "YamlDocLexerValueCollection",
    "YamlDocLexerValueList"
]
