from __future__ import annotations

from contextlib import AbstractContextManager
from dataclasses import dataclass
from typing import Final
from typing import Generic
from typing import Protocol

from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import ScalarT
from pygentype.abc import T
from pygentype.abc import Type


class TestExceptionCatcher(AbstractContextManager[object], Protocol):
    exception: Exception


@dataclass
class TestAssertEqualMsgArgs(Generic[T], DataclassInstance):
    expected: Final[T]
    actual: Final[T]


class TestAssertEqualMsg(Protocol, Generic[T]):
    def __call__(self, args: TestAssertEqualMsgArgs[T]) -> str:
        raise SystemException()


TestAssertExceptionEqualMsgArgs = TestAssertEqualMsgArgs[Exception]
TestAssertExceptionEqualMsg = TestAssertEqualMsg[Exception]
TestAssertObjectEqualMsgArgs = TestAssertEqualMsgArgs[object]
TestAssertObjectEqualMsg = TestAssertEqualMsg[object]
TestAssertScalarEqualMsgArgs = TestAssertEqualMsgArgs[object]
TestAssertScalarEqualMsg = TestAssertEqualMsg[object]


@dataclass
class TestAssertTextEqualMsgArgs(DataclassInstance):
    expected: Final[str]
    actual: Final[str]
    diff: Final[str]


class TestAssertTextEqualMsg(Protocol):
    def __call__(self, args: TestAssertTextEqualMsgArgs) -> str:
        raise SystemException()


class Test(Protocol):
    def assert_exception_equal(
        self,
        expected: Exception,
        exception_catcher: TestExceptionCatcher,
        msg: TestAssertExceptionEqualMsg = ...
    ) -> None:
        raise SystemException()

    def assert_object_equal(
        self,
        expected: object,
        actual: object,
        msg: TestAssertObjectEqualMsg = ...
    ) -> None:
        raise SystemException()

    def assert_scalar_equal(
        self,
        expected: ScalarT,
        actual: ScalarT,
        msg: TestAssertScalarEqualMsg = ...
    ) -> None:
        raise SystemException()

    def assert_text_equal(
        self,
        expected: str,
        actual: str,
        msg: TestAssertTextEqualMsg = ...
    ) -> None:
        raise SystemException()

    def exception_catcher(
        self,
        exception_type: Type[Exception] = ...
    ) -> TestExceptionCatcher:
        raise SystemException()

    def sub_test(self, msg: str) -> AbstractContextManager[None]:
        raise SystemException()


class TestCase(object):
    def run(self, test: Test) -> None:
        raise SystemException()


class TestSuite(object):
    def add_test_case(self, test_case: TestCase) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Test",
    "TestAssertExceptionEqualMsg",
    "TestAssertExceptionEqualMsgArgs",
    "TestAssertObjectEqualMsg",
    "TestAssertObjectEqualMsgArgs",
    "TestAssertScalarEqualMsg",
    "TestAssertScalarEqualMsgArgs",
    "TestAssertTextEqualMsg",
    "TestAssertTextEqualMsgArgs",
    "TestCase",
    "TestExceptionCatcher",
    "TestSuite"
]
