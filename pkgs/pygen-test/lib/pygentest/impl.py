from __future__ import annotations

from pygentest.abc import TestAssertExceptionEqualMsg
from pygentest.abc import TestAssertExceptionEqualMsgArgs
from pygentest.abc import TestAssertObjectEqualMsg
from pygentest.abc import TestAssertObjectEqualMsgArgs
from pygentest.abc import TestAssertScalarEqualMsgArgs
from pygentest.abc import TestAssertTextEqualMsg
from pygentest.abc import TestAssertTextEqualMsgArgs
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class TestAssertExceptionEqualMsgImpl(TestAssertExceptionEqualMsg):
    def __call__(self, args: TestAssertExceptionEqualMsgArgs) -> str:
        return "expected exception != actual exception"


class TestAssertObjectEqualMsgImpl(TestAssertObjectEqualMsg):
    def __call__(self, args: TestAssertObjectEqualMsgArgs) -> str:
        return "expected object != actual object"


class TestAssertScalarEqualMsgImpl(object):
    def __call__(self, args: TestAssertScalarEqualMsgArgs) -> str:
        return TextUtils.format_args(
            tpl=(
                "expected value != actual value\n"
                "  e: {}\n"
                "  a: {}"
            ),
            args=[
                TextUtils.to_safe_text(args.expected),
                TextUtils.to_safe_text(args.actual)
            ]
        )


class TestAssertTextEqualMsgImpl(TestAssertTextEqualMsg):
    def __call__(self, args: TestAssertTextEqualMsgArgs) -> str:
        return TextUtils.format_args(
            tpl="expected text != actual text\n{}",
            args=[
                args.diff
            ]
        )


__all__: FinalCollection[str] = [
    "TestAssertExceptionEqualMsgImpl",
    "TestAssertObjectEqualMsgImpl",
    "TestAssertScalarEqualMsgImpl",
    "TestAssertTextEqualMsgImpl"
]
