from __future__ import annotations

from pygenerr.err import UserException
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class ArgEmptyValueException(UserException):
    @classmethod
    def create(cls, description: str) -> Exception:
        return cls(
            msg=TextUtils.format_args(
                tpl="[{}]: empty value",
                args=[
                    description
                ]
            )
        )


__all__: FinalCollection[str] = [
    "ArgEmptyValueException"
]
