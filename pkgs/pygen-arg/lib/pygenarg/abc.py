from __future__ import annotations

from contextlib import AbstractContextManager
from typing import Final
from typing import Generic
from typing import TypeVar

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import T

ArgContext = TypeVar(
    name="ArgContext",
    covariant=True
)


class Arg(Generic[ArgContext], AbstractContextManager[ArgContext]):
    def __enter__(self) -> ArgContext:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()

    def describe(self) -> str:
        raise SystemException()


FinalArg = Final[Arg[T]]


__all__: FinalCollection[str] = [
    "Arg",
    "FinalArg"
]
