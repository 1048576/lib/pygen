from __future__ import annotations

from typing import Final

from pygenalphabet.abc import Alphabet
from pygenalphabet.impl import AlphabetImpl
from pygencollection.impl import ArrayImpl
from pygentype.abc import FinalCollection


class AlphabetUtils(object):
    LATIN_LOWERCASE: Final[Alphabet] = AlphabetImpl(
        ranges=ArrayImpl.of(
            range(ord("a"), ord("z") + 1)
        )
    )
    LATIN: Final[Alphabet] = AlphabetImpl(
        ranges=ArrayImpl.of(
            # range(ord("A"), ord("Z") + 1),
            # range(ord("a"), ord("z") + 1)
            range(ord("A"), ord("z") + 1)
        )
    )
    NUMBERS: Final[Alphabet] = AlphabetImpl(
        ranges=ArrayImpl.of(
            range(ord("0"), ord("9") + 1)
        )
    )


__all__: FinalCollection[str] = [
    "AlphabetUtils"
]
