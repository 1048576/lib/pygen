from __future__ import annotations

from collections.abc import Iterable

from pygenalphabet.abc import Alphabet
from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Range


class AlphabetImpl(Alphabet):
    _ranges: FinalCollection[Range]

    def __init__(self, ranges: Iterable[Range]) -> None:
        self._ranges = ArrayImpl.copy(ranges)

    def __contains__(self, char: str) -> bool:
        return CollectionUtils.any(
            items=self._ranges,
            fn=lambda range: (ord(char) in range)
        )


__all__: FinalCollection[str] = [
    "AlphabetImpl"
]
