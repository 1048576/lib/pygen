from __future__ import annotations

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class Alphabet(object):
    def __contains__(self, char: str) -> bool:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Alphabet"
]
