from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenpath.abc import Filepath
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class UnknownFileTestCaseImpl(TestCase):
    _filepaths: FinalCollection[Filepath]

    def __init__(self, filepaths: Iterable[Filepath]) -> None:
        self._filepaths = ArrayImpl.copy(filepaths)

    def run(self, test: Test) -> None:
        filepaths = CollectionUtils.head(
            items=self._filepaths,
            size=10
        )

        test.assert_text_equal(
            expected="",
            actual=TextUtils.join(
                delimiter="\n",
                items=CollectionUtils.map(
                    items=filepaths,
                    fn=lambda filepath: filepath.path()
                )
            ),
            msg=lambda args: TextUtils.format_args(
                tpl="Unknown files\n{}\n...",
                args=[
                    args.diff
                ]
            )
        )


__all__: FinalCollection[str] = [
    "UnknownFileTestCaseImpl"
]
